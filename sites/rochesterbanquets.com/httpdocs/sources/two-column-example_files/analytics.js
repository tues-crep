/* Track outgoing clicks */
//
//  By default, this script will track links to the follwoing file 
//  types: .doc, .xls, .exe, .zip and .pdf
//
var fileTypes    = (".doc",".xls",".exe",".zip",".pdf");

// This is a debug mode flag.  Change to '' for production.  In debug mode
// the script will display an alert box and skip sending data to Google 
// Analytics.
//
var debug          = '';

//
// This variable controls how outbond links will appear
// the GA reports.  By default, external links will appear as
// '/outbound/<URL>' where URL is the URL in the anchor tag.
//
var extIdentifier  = '/outbound'+_udl.pathname+'/'+_udl.search+'/';

/// No need to change anything below this line ///

if (document.getElementsByTagName) { 
  // Initialize external link handlers 
  var hrefs = document.getElementsByTagName('a'); 

  for (var l = 0; l < hrefs.length; l++) { 
    //protocol, host, hostname, port, pathname, search, hash 
    if (hrefs[l].hostname == location.host) { 
      var path = hrefs[l].pathname; 
    if (path.indexOf(fileTypes) != -1) startListening(hrefs[l],"click",trackDocuments); 
    } else { 
      startListening(hrefs[l],"click",trackExternalLinks); 
    } 
  } 
} 

function startListening (obj,evnt,func) { 
  if (obj.addEventListener) {
    obj.addEventListener(evnt,func,false); 
  } else if (obj.attachEvent) {
    obj.attachEvent("on" + evnt,func); 
  } 
} 

function trackDocuments (evnt) { 
  var url = (evnt.srcElement) ? "/" + evnt.srcElement.pathname : this.pathname; 

  if (typeof(urchinTracker) == "function") {
	  if (!debug) {
		  urchinTracker(url);
	  } else { 
		  alert(url);
		  return false;
	  }
  }
} 

function trackExternalLinks (evnt) { 
  var lnk; 
  if (evnt.srcElement) { 
    var elmnt = evnt.srcElement; 
    while (elmnt.tagName != "A") { 
      var newelmnt = elmnt.parentNode; 
      elmnt = newelmnt; 
    } 
    lnk = extIdentifier +elmnt.hostname + "/" + elmnt.pathname + elmnt.search; 
  } else { 
    lnk = extIdentifier + this.hostname + this.pathname + this.search; 
  } 

  if (typeof(urchinTracker) == "function") {
	  if (!debug) {
		  urchinTracker(lnk);
	  } else {
		  alert(lnk);
		  return false;
	  }
  }
}

/*Div. code */
_uacct = "UA-79784-5";
_uOsr.push("images.google");
_uOkw.push("prev");