<hr/>
<a name="contact">&nbsp;</a>
<h3>To contact us:</h3>
<p>The River's Edge<br>
31 Paul Road at Scottsville Road<br>
Rochester, NY 14624</p>
<table style="width: 100%" cellspacing="0" cellpadding="0">
<tr><td><p>Phone: 585-235-3630<br>
Fax: 585-235-3637<br>
E-mail: <a href="mailto:theriversedge@frontiernet.net">theriversedge@frontiernet.net</a>
</p></td>
<td style="text-align: right">Handicap Accessible<br/><img src="images/handicap.jpg" width="40" height="40"/></td></tr></table>
