<!--
This is the navigation menu.  This is available in the left sidebar when navigating from page to page
and in the footer always, even when surfing the bar and buffet menus.
-->
<ul>
    <li><a class="first" href=".">Home</a></li>
    <li><a href="menu.php">Menus</a></li>
	<li><a target="_blank" href="images/Marinara.pdf">Mazza's Marinara</a></li>
	<li><a target="_blank" href="images/Cafe.pdf">RE Caf&eacute; @ Unity</a></li>
	<li><a href="./#contact">Contact Us</a></li>
    <li><a href="./map.php">Map &amp; Directions</a></li>
</ul>
