<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"><head><title>River's Edge Party House</title>

<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=iso-8859-1">
<meta http-equiv="imagetoolbar" content="no">

<link rel="stylesheet" type="text/css" href="main.css">
</style></head><body>
<div id="mainContainer">
<div id="header">
<table style="width: 100%; height: 126px;">
  <tr>
    <td align="center" valign="top">
	  <h1>The River's Edge<br/>Party House</h1>
	</td>
	<td align="right" valign="top" width="405">
	  <img height="126" width="405" src="images/header-photo.png" />
	</td>
  </tr>
</table>
</div> 
<div class="outer">
<div class="inner">
<div class="float-wrap">
        <div id="content"> 
          <div class="contentWrap"> 
		   <table width="624">
		     <tr>
			   <th colspan="2" width="624" align="center">
			   <h2 style="font-size: 1.9em; color: black; font-family: arial">Menu Offerings &amp; Bar Packages</h2>
			   </th>
			 </tr>
			 <tr>
			   <td rowspan="2" width="312" valign="top" align="center">
			   <div style="display: block; width: 300px; border: 4px double black; min-height: 350px;">
				<h3 style="color: black; font-family: arial; font-size: 10.5pt">Menus</h3><h6>(Click to View)</h6>
				<ul>
					<li><a href="images/menus/1_basic.pdf" target="_blank">Basic Buffet</a></li>
					<li><a href="images/menus/2_casual.pdf" target="_blank">Casual Buffet</a></li>
					<li><a href="images/menus/3_classic.pdf" target="_blank">Classic Buffet</a></li>
					<li><a href="images/menus/4_traditional.pdf" target="_blank">Traditional Buffet</a></li>
					<li><a href="images/menus/5_banquet.pdf" target="_blank">The Banquet Package</a></li>
					<li><a href="images/menus/6_bridal.pdf" target="_blank">The Bridal Package</a></li>
					<li><a href="images/Served.pdf" target="_blank">Served Dinners</a></li>
					<li><a href="images/menus/7_hors_cruds.pdf" target="_blank">The Hors D'oeuvre Menu<br/>Crudit&eacute;s Station</a></li>
					<li><a href="images/menus/8_brunch.pdf" target="_blank">Brunch Buffet</a></li>
					<li><a href="images/menus/9_lunch.pdf" target="_blank">Lunch Buffet</a></li>
					<li><a href="images/Seminars.pdf" target="_blank">Meetings &amp; Seminars</a></li>
					<li><a href="images/menus/10_sweets.pdf" target="_blank">Sweet Finales</a></li>
				</ul>
			   </div>
			   </td>
			   <td width="312" valign="top" align="center">
			   <div style="display: block; width: 300px; border: 4px double black; min-height: 130px;">
				<h3 style="color: black; font-family: arial; font-size: 10.5pt">Bar Packages</h3><h6>(Click to View)</h6>
				<ul>
					<a href="images/menus/11_bar.pdf" target="_blank">
					<li>Deluxe Bar Package</li>
					<li>Standard Bar Package</li>
					<li>Traditional Bar</li>
					</a>
				</ul>
			   </div>
			   </td>
			 </tr>
			 <tr>
			   <td width="312" align="center" valign="top">
			     <img src="images/menu-photo.png" width="300" height="200"/>
			   </td>
			 </tr>
           </table>	
<?php
include('contact.php');
?>		   
		  </div>
		</div>
        <!-- end centered div -->
        <div id="left"> 
<?php
include('links.php');
?>
        </div>
<!-- end left div -->

<div class="clear"></div> 
</div>
</div>
</div>
<div id="footer">
<?php
include('links.php');
?>
</div>

<!-- If you copy the HTML on this page, do NOT copy the code below!
Thanks.
-->
<script src="two-column-example_files/mlt.js" type="text/javascript"></script><img src="two-column-example_files/mlt.gif" height="1" width="1">
<!-- End Do NOT copy HTML -->

</body></html>