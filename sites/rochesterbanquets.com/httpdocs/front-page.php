<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"><head><title>River's Edge Party House</title>

<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=iso-8859-1">
<meta http-equiv="imagetoolbar" content="no">

<link rel="stylesheet" type="text/css" href="main.css">
</style></head><body>
<div id="mainContainer">
<div id="header">
<table style="width: 100%; height: 126px;">
  <tr>
    <td align="center" valign="top">
	  <h1><em>The River's Edge<br/>Party House</em></h1>
	</td>
	<td align="right" valign="top" width="405">
	  <img height="126" width="405" src="images/header-photo.png" />
	</td>
  </tr>
</table>
</div> 
<div class="outer">
<div class="inner">
<div class="float-wrap">
        <div id="content"> 
          <div class="contentWrap"> 
		    <div style="text-align: center">
<p>It's never too late to schedule your weekday meeting or seminar.  Check out our new page of <a href="images/Seminars.pdf">Meeting &amp; Seminar</a> Options.</p>
<hr/>
<h2 style="font-weight: normal;"><em>Now Booking Events Seven Days A Week</em></h2>
<hr/>
<p>The River's Edge Party House is booking dates throughout 2008 and into 2009.<br/>
We are offering special rates for events held on most Fridays &amp; Sundays.<br/>
Please contact our Event Coordinator Pattie Smith for details.</p>
<p>For your convenience, we have all of our menu options available <a href="menu.php">here</a>.</p>
             </div>
<?php
include('contact.php');
?>
			</div>
		  </div>
        <!-- end centered div -->
        <div id="left"> 
<?php
include('links.php');
?>
        </div>
<!-- end left div -->

<div class="clear"></div> 
</div>
</div>
</div>
<div id="footer">
<?php
include('links.php');
?>
</div>

<!-- If you copy the HTML on this page, do NOT copy the code below!
Thanks.
-->
<script src="two-column-example_files/mlt.js" type="text/javascript"></script><img src="two-column-example_files/mlt.gif" height="1" width="1">
<!-- End Do NOT copy HTML -->

</body></html>
