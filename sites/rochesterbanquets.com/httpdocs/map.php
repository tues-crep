<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"><head><title>River's Edge Party House</title>

<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=iso-8859-1">
<meta http-equiv="imagetoolbar" content="no">

<link rel="stylesheet" type="text/css" href="main.css">
</style></head><body>
<div id="mainContainer">
<div id="header">
<table style="width: 100%; height: 126px;">
  <tr>
    <td align="center" valign="top">
	  <h1><em>The River's Edge<br/>Party House</em></h1>
	</td>
	<td align="right" valign="top" width="405">
	  <img height="126" width="405" src="images/header-photo.png" />
	</td>
  </tr>
</table>
</div> 
<div class="outer">
<div class="inner">
<div class="float-wrap">
        <div id="content"> 
          <div class="contentWrap"> 
<h3>Directions:</h3>
<p>The River's Edge Party House is located at the south end of
the Greater Rochester International Airport.</p>
<ul>
<li>Take a right off Exit 17 / Scottsville Road</li>
<li>Travel 1.5 miles to the light at Paul Road</li>
<li>Turn right on Paul and then the first left into our parking lot.</li>
</ul>
<hr/>
<p style="text-align: center"><img src="images/mapquest.jpg"/></p>
<?php
include('contact.php');
?>
			</div>
		  </div>
        <!-- end centered div -->
        <div id="left"> 
<?php
include('links.php');
?>
        </div>
<!-- end left div -->

<div class="clear"></div> 
</div>
</div>
</div>
<div id="footer">
<?php
include('links.php');
?>
</div>

<!-- If you copy the HTML on this page, do NOT copy the code below!
Thanks.
-->
<script src="two-column-example_files/mlt.js" type="text/javascript"></script><img src="two-column-example_files/mlt.gif" height="1" width="1">
<!-- End Do NOT copy HTML -->

</body></html>


