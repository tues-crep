import java.util.*;
import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.concurrent.*;


/*
 * Server.java
 *
 * Created on January 13, 2007, 8:45 PM
 *
 * The server executable for the checkers game
 */

/**
 *
 * @author Austin
 */
public class Server {
    
    //Map of the Games
    private HashMap gameMap;
    
    //Set of players playing a game
    private HashSet playSet;
    
    //Map of players waiting.  key is waiting player, value is player waited on
    private HashMap waitMap;
    
    
    private int TCPport; // The port for the TCP server
    
    private int UDPport; // The port for the UDP server
    
    /**
     * Initializes a game between two players using first players connection
     *
     * @param p1Name Initiating player's name
     * @param p2Name Joining player's name
     * @param p1Connection Initiating player's connection
     **/
    public void initGame(String p1Name, String p2Name, BaseConnection p1Connection) {
        System.out.println("InitGame");
        
        // Ensure initiator isn't playing other games'
        if((!waitMap.containsKey(p1Name))
        && (!p1Name.equals(p2Name))
        && (!playSet.contains(p1Name))) {
            
            
            if(waitMap.containsKey(p2Name)) {
                if(waitMap.get(p2Name).equals(p1Name)) {
                    joinGame(p2Name, p1Name, p1Connection);
                } else {
                    // Can't play due to other player waiting for someone else
                    p1Connection.sendData("a");
                }
            } else if(playSet.contains(p2Name)) {
                // Can't play since other player is already playing with someone
                
            } else {
                //initGame
                createGame(p1Name, p2Name, p1Connection);
            }
            
        } else {
            // Send an abort for unsuccessful join
            p1Connection.sendData("a");
        }
        System.out.println("InitEnd");
    }
    
    
    /**
     * End a game by removing players and game from the server
     *
     * @param p1Name first player's name
     * @param p2Name second player's name
     * @param gameName game's name
     **/
    public void endGame(String p1Name, String p2Name, String gameName) {
        playSet.remove(p1Name);
        playSet.remove(p2Name);
        gameMap.remove(gameName);
    }
    
    
    /**
     * Join player to other player's game
     *
     * @param p1Name joining player's name
     * @param p2Name waiting player's name
     * @param p1Connection joining player's connection
     */
    public void joinGame(String p1Name, String p2Name, BaseConnection p1Connection) {
        System.out.println("Joining Game");
        
        //Get the game and create it's thread
        ServerGame aGame = (ServerGame)gameMap.get(p1Name + "-" + p2Name);
        Thread aThread = new Thread(aGame);
        
        // Join the player to the game
        aGame.joinGame(p1Connection);
        
        // Start the game
        aThread.start();
        
        // Remove players from the waitMap and add them to the playSet
        waitMap.remove(p1Name);
        waitMap.remove(p2Name);
        playSet.add(p1Name);
        playSet.add(p2Name);
        
        System.out.println(p1Name + " joined game " + p1Name + "-" + p2Name);
    }
    
    
    /**
     * Creates a new game and updates relivent data
     *
     * @param p1Name Initiating player's name.
     * @param p2Name Other player's name.
     * @param p1Connection Initiating player's connection
     **/
    public void createGame(String p1Name, String p2Name, BaseConnection p1Connection) {
        System.out.println("CreatingGame");
        
        ServerGame aGame = new ServerGame(p1Name + "-" + p2Name, p1Name, p2Name, p1Connection);
        gameMap.put(p1Name + "-" + p2Name, aGame);
        
        waitMap.put(p1Name, p2Name);
        System.out.println(p1Name + "Created Game" + p1Name + "-" + p2Name);
        p1Connection.sendData("w");
    }
    
    
    /**
     * Determines if a game between two players exists
     *
     * @param p1 - First player's name
     *
     * @param p2 - Second player's name
     *
     * @retval boolean indicating if a game between the players exists
     */
    public boolean gameExists(String p1, String p2) {
        if(gameMap.containsKey(p1 + "-" + p2) || gameMap.containsKey(p2 + "-" + p1)) {
            return true;
        } else {
            return false;
        }
    }
    
    
    /**
     * Default constructor
     **/
    public Server() {
    }
    
    
    /**
     * Constructor that initiates the UDP and TCP ports
     *
     * @param TCPPort - The port the TCP server will use.
     *
     * @param UDPport - The port the UDP server will use.
     **/
    public Server(int TCPport, int UDPport) {
        this.TCPport = TCPport;
        this.UDPport = UDPport;
    }
    
    
    /**
     * Main Method
     **/
    public static void main(String args[]) {
        
        // Tries to create a new server
        try {
            // Creates an instance of server and initializes storage maps/sets
            Server Me = new Server(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
            Me.playSet = new HashSet();
            Me.gameMap = new HashMap();
            Me.waitMap = new HashMap();
            
            // Creates and starts the TCP server
            Thread tcpServe = new Thread(new TCPInit(Me, Me.TCPport));
            tcpServe.start();
            
            // Creates and starts the UDP server
            //Thread udpServe = new Thread(new UDPInit(this, UDPport));
            //udpServe.start();
        } catch (Exception e) {
            System.out.println("E" + e.getMessage());
        }
    }
}
