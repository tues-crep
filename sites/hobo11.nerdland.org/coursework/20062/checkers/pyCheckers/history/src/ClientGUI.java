import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.util.Map;
import java.util.LinkedList;

/*
 * ClientGUI.java
 *
 * Created on January 13, 2007, 11:06 PM
 */

/**
 *
 * @author  Austin
 */
public class ClientGUI extends javax.swing.JFrame {
    
    PlayerGame theGame; // The game this is related with
    
    String myName; // Player's name
    
    String opName; // Opponent's name'
    
    char myPiece; // Player's piece symbol
    
    char opPiece; // Opponent's piece symbol
    
    char myKing; // Player's king symbol
    
    char opKing; // Opponent's king symbol
    
    String myIcon; // Player's small icon;
    
    String opIcon; // Opponent's small icon
    
    String myPieceIcon; // Player's piece icon
    
    String opPieceIcon; // Opponent's piece icon
    
    String myKingIcon; // Player's king icon
    
    String opKingIcon; // Opponent's king icon
    
    String gameName; // Game name
    
    Jump myJump;
    
    // Game board containing the buttons representing the spaces
    javax.swing.JButton[][] myBoard;
    
    
    final String red = "r.JPG";
    final String red_k = "r_k.JPG";
    final String black = "b.JPG";
    final String black_k = "b_k.JPG";
    final String red_24 = "r_24.JPG";
    final String black_24 = "b_24.JPG";
    
    
    /** Creates new form ClientGUI */
    public ClientGUI(String myName, String opponentsName, String gameName, char myColor, PlayerGame theGame) {
        initComponents();
        
        this.theGame = theGame;
        this.myPiece = myColor;
        this.myName = myName;
        this.opName = opponentsName;
        this.gameName = gameName;
        myBoard = new javax.swing.JButton[8][8];
        if(myColor == 'r') {
            opPiece = 'b';
            myKingIcon = red_k;
            opKingIcon = black_k;
            myIcon = red_24;
            opIcon = black_24;
            myKing = 'R';
            opKing = 'B';
            myPieceIcon = red;
            opPieceIcon = black;
            
            this.turn_label.setText(myName);
        } else {
            opPiece = 'r';
            myKingIcon = black_k;
            opKingIcon = red_k;
            myIcon = black_24;
            opIcon = red_24;
            myKing = 'B';
            opKing = 'R';
            myPieceIcon = black;
            opPieceIcon = red;
            
            this.turn_label.setText(opponentsName);
        }
        
        this.my_icon.setIcon(new javax.swing.ImageIcon(myIcon));
        this.opponent_icon.setIcon(new javax.swing.ImageIcon(opIcon));
        this.my_label.setText(myName);
        this.opponent_label.setText(opName);
        this.turn_icon.setIcon(new javax.swing.ImageIcon(red_24));
        setTitle("Checkers - " + this.gameName);
        
        // Load board with coordinating buttons
        myBoard[0][0] = s00;
        myBoard[0][0] = s00;
        myBoard[0][1] = s01;
        myBoard[0][2] = s02;
        myBoard[0][3] = s03;
        myBoard[0][4] = s04;
        myBoard[0][5] = s05;
        myBoard[0][6] = s06;
        myBoard[0][7] = s07;
        myBoard[1][0] = s10;
        myBoard[1][1] = s11;
        myBoard[1][2] = s12;
        myBoard[1][3] = s13;
        myBoard[1][4] = s14;
        myBoard[1][5] = s15;
        myBoard[1][6] = s16;
        myBoard[1][7] = s17;
        myBoard[2][0] = s20;
        myBoard[2][1] = s21;
        myBoard[2][2] = s22;
        myBoard[2][3] = s23;
        myBoard[2][4] = s24;
        myBoard[2][5] = s25;
        myBoard[2][6] = s26;
        myBoard[2][7] = s27;
        myBoard[3][0] = s30;
        myBoard[3][1] = s31;
        myBoard[3][2] = s32;
        myBoard[3][3] = s33;
        myBoard[3][4] = s34;
        myBoard[3][5] = s35;
        myBoard[3][6] = s36;
        myBoard[3][7] = s37;
        myBoard[4][0] = s40;
        myBoard[4][1] = s41;
        myBoard[4][2] = s42;
        myBoard[4][3] = s43;
        myBoard[4][4] = s44;
        myBoard[4][5] = s45;
        myBoard[4][6] = s46;
        myBoard[4][7] = s47;
        myBoard[5][0] = s50;
        myBoard[5][1] = s51;
        myBoard[5][2] = s52;
        myBoard[5][3] = s53;
        myBoard[5][4] = s54;
        myBoard[5][5] = s55;
        myBoard[5][6] = s56;
        myBoard[5][7] = s57;
        myBoard[6][0] = s60;
        myBoard[6][1] = s61;
        myBoard[6][2] = s62;
        myBoard[6][3] = s63;
        myBoard[6][4] = s64;
        myBoard[6][5] = s65;
        myBoard[6][6] = s66;
        myBoard[6][7] = s67;
        myBoard[7][0] = s70;
        myBoard[7][1] = s71;
        myBoard[7][2] = s72;
        myBoard[7][3] = s73;
        myBoard[7][4] = s74;
        myBoard[7][5] = s75;
        myBoard[7][6] = s76;
        myBoard[7][7] = s77;
        
        int i, j;
        
        // Set up pieces
        for(i = 0;i < 8;i++) {
            // Set up red
            for(j = 0;j<3;j++) {
                if(((i+j)%2) != 0) {
                    myBoard[i][j].setIcon(new javax.swing.ImageIcon(red));
                    myBoard[i][j].setText("r");
                }
            }
            
            // Set up black
            for(j = 5;j<8;j++) {
                if(((i+j)%2) != 0) {
                    myBoard[i][j].setIcon(new javax.swing.ImageIcon(black));
                    myBoard[i][j].setText("b");
                }
            }
        }
        
        // Disable white spaces
        for(i=0;i<8;i++) {
            for(j=0;j<8;j++) {
                if(((i+j)%2) == 0) {
                    myBoard[i][j].setEnabled(false);
                }
            }
        }
    }
    
    public ClientGUI(String myName, String opponentsName, String gameName, char myColor) {
        initComponents();
        
        this.theGame = theGame;
        this.myPiece = myColor;
        this.myName = myName;
        this.opName = opponentsName;
        this.gameName = gameName;
        myBoard = new javax.swing.JButton[8][8];
        if(myColor == 'r') {
            opPiece = 'b';
            myKingIcon = red_k;
            opKingIcon = black_k;
            myIcon = red_24;
            opIcon = black_24;
            myKing = 'R';
            opKing = 'B';
            myPieceIcon = red;
            opPieceIcon = black;
            
            this.turn_label.setText(myName);
        } else {
            opPiece = 'r';
            myKingIcon = black_k;
            opKingIcon = red_k;
            myIcon = black_24;
            opIcon = red_24;
            myKing = 'B';
            opKing = 'R';
            myPieceIcon = black;
            opPieceIcon = red;
            
            this.turn_label.setText(opponentsName);
        }
        
        this.my_icon.setIcon(new javax.swing.ImageIcon(myIcon));
        this.opponent_icon.setIcon(new javax.swing.ImageIcon(opIcon));
        this.my_label.setText(myName);
        this.opponent_label.setText(opName);
        this.turn_icon.setIcon(new javax.swing.ImageIcon(red_24));
        setTitle("Checkers - " + this.gameName);
        
        // Load board with coordinating buttons
        myBoard[0][0] = s00;
        myBoard[0][0] = s00;
        myBoard[0][1] = s01;
        myBoard[0][2] = s02;
        myBoard[0][3] = s03;
        myBoard[0][4] = s04;
        myBoard[0][5] = s05;
        myBoard[0][6] = s06;
        myBoard[0][7] = s07;
        myBoard[1][0] = s10;
        myBoard[1][1] = s11;
        myBoard[1][2] = s12;
        myBoard[1][3] = s13;
        myBoard[1][4] = s14;
        myBoard[1][5] = s15;
        myBoard[1][6] = s16;
        myBoard[1][7] = s17;
        myBoard[2][0] = s20;
        myBoard[2][1] = s21;
        myBoard[2][2] = s22;
        myBoard[2][3] = s23;
        myBoard[2][4] = s24;
        myBoard[2][5] = s25;
        myBoard[2][6] = s26;
        myBoard[2][7] = s27;
        myBoard[3][0] = s30;
        myBoard[3][1] = s31;
        myBoard[3][2] = s32;
        myBoard[3][3] = s33;
        myBoard[3][4] = s34;
        myBoard[3][5] = s35;
        myBoard[3][6] = s36;
        myBoard[3][7] = s37;
        myBoard[4][0] = s40;
        myBoard[4][1] = s41;
        myBoard[4][2] = s42;
        myBoard[4][3] = s43;
        myBoard[4][4] = s44;
        myBoard[4][5] = s45;
        myBoard[4][6] = s46;
        myBoard[4][7] = s47;
        myBoard[5][0] = s50;
        myBoard[5][1] = s51;
        myBoard[5][2] = s52;
        myBoard[5][3] = s53;
        myBoard[5][4] = s54;
        myBoard[5][5] = s55;
        myBoard[5][6] = s56;
        myBoard[5][7] = s57;
        myBoard[6][0] = s60;
        myBoard[6][1] = s61;
        myBoard[6][2] = s62;
        myBoard[6][3] = s63;
        myBoard[6][4] = s64;
        myBoard[6][5] = s65;
        myBoard[6][6] = s66;
        myBoard[6][7] = s67;
        myBoard[7][0] = s70;
        myBoard[7][1] = s71;
        myBoard[7][2] = s72;
        myBoard[7][3] = s73;
        myBoard[7][4] = s74;
        myBoard[7][5] = s75;
        myBoard[7][6] = s76;
        myBoard[7][7] = s77;
        
        int i, j;
        
        // Set up pieces
        for(i = 0;i < 8;i++) {
            // Set up red
            for(j = 0;j<3;j++) {
                if(((i+j)%2) != 0) {
                    myBoard[i][j].setIcon(new javax.swing.ImageIcon(red));
                    myBoard[i][j].setText("r");
                }
            }
            
            // Set up black
            for(j = 5;j<8;j++) {
                if(((i+j)%2) != 0) {
                    myBoard[i][j].setIcon(new javax.swing.ImageIcon(black));
                    myBoard[i][j].setText("b");
                }
            }
        }
        
        // Disable white spaces
        for(i=0;i<8;i++) {
            for(j=0;j<8;j++) {
                if(((i+j)%2) == 0) {
                    myBoard[i][j].setEnabled(false);
                }
            }
        }
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        s10 = new javax.swing.JButton();
        s11 = new javax.swing.JButton();
        s00 = new javax.swing.JButton();
        s01 = new javax.swing.JButton();
        s02 = new javax.swing.JButton();
        s03 = new javax.swing.JButton();
        s04 = new javax.swing.JButton();
        s05 = new javax.swing.JButton();
        s06 = new javax.swing.JButton();
        s07 = new javax.swing.JButton();
        s12 = new javax.swing.JButton();
        s13 = new javax.swing.JButton();
        s14 = new javax.swing.JButton();
        s15 = new javax.swing.JButton();
        s16 = new javax.swing.JButton();
        s17 = new javax.swing.JButton();
        s70 = new javax.swing.JButton();
        s71 = new javax.swing.JButton();
        s50 = new javax.swing.JButton();
        s51 = new javax.swing.JButton();
        s40 = new javax.swing.JButton();
        s41 = new javax.swing.JButton();
        s30 = new javax.swing.JButton();
        s31 = new javax.swing.JButton();
        s20 = new javax.swing.JButton();
        s21 = new javax.swing.JButton();
        s60 = new javax.swing.JButton();
        s61 = new javax.swing.JButton();
        s42 = new javax.swing.JButton();
        s43 = new javax.swing.JButton();
        s44 = new javax.swing.JButton();
        s32 = new javax.swing.JButton();
        s33 = new javax.swing.JButton();
        s34 = new javax.swing.JButton();
        s22 = new javax.swing.JButton();
        s23 = new javax.swing.JButton();
        s24 = new javax.swing.JButton();
        s62 = new javax.swing.JButton();
        s63 = new javax.swing.JButton();
        s52 = new javax.swing.JButton();
        s53 = new javax.swing.JButton();
        s72 = new javax.swing.JButton();
        s73 = new javax.swing.JButton();
        s74 = new javax.swing.JButton();
        s64 = new javax.swing.JButton();
        s54 = new javax.swing.JButton();
        s25 = new javax.swing.JButton();
        s35 = new javax.swing.JButton();
        s45 = new javax.swing.JButton();
        s55 = new javax.swing.JButton();
        s65 = new javax.swing.JButton();
        s75 = new javax.swing.JButton();
        s26 = new javax.swing.JButton();
        s27 = new javax.swing.JButton();
        s36 = new javax.swing.JButton();
        s37 = new javax.swing.JButton();
        s46 = new javax.swing.JButton();
        s47 = new javax.swing.JButton();
        s76 = new javax.swing.JButton();
        s66 = new javax.swing.JButton();
        s56 = new javax.swing.JButton();
        s67 = new javax.swing.JButton();
        s57 = new javax.swing.JButton();
        s77 = new javax.swing.JButton();
        my_icon = new javax.swing.JLabel();
        my_label = new javax.swing.JLabel();
        opponent_label = new javax.swing.JLabel();
        opponent_icon = new javax.swing.JLabel();
        turn_icon = new javax.swing.JLabel();
        turn_label = new javax.swing.JLabel();

        getContentPane().setLayout(new java.awt.GridBagLayout());

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setName("ClientFrame");
        setResizable(false);
        jPanel1.setLayout(new java.awt.GridBagLayout());

        jPanel1.setMaximumSize(new java.awt.Dimension(400, 400));
        jPanel1.setMinimumSize(new java.awt.Dimension(400, 400));
        jPanel1.setPreferredSize(new java.awt.Dimension(400, 400));
        s10.setBackground(new java.awt.Color(0, 0, 0));
        s10.setPreferredSize(new java.awt.Dimension(48, 48));
        s10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s10MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        jPanel1.add(s10, gridBagConstraints);

        s11.setBackground(new java.awt.Color(255, 255, 255));
        s11.setForeground(new java.awt.Color(255, 255, 255));
        s11.setEnabled(false);
        s11.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        jPanel1.add(s11, gridBagConstraints);

        s00.setBackground(new java.awt.Color(255, 255, 255));
        s00.setForeground(new java.awt.Color(255, 255, 255));
        s00.setEnabled(false);
        s00.setPreferredSize(new java.awt.Dimension(48, 48));
        jPanel1.add(s00, new java.awt.GridBagConstraints());

        s01.setBackground(new java.awt.Color(0, 0, 0));
        s01.setPreferredSize(new java.awt.Dimension(48, 48));
        s01.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s01MouseClicked(evt);
            }
        });

        jPanel1.add(s01, new java.awt.GridBagConstraints());

        s02.setBackground(new java.awt.Color(255, 255, 255));
        s02.setForeground(new java.awt.Color(255, 255, 255));
        s02.setEnabled(false);
        s02.setPreferredSize(new java.awt.Dimension(48, 48));
        jPanel1.add(s02, new java.awt.GridBagConstraints());

        s03.setBackground(new java.awt.Color(0, 0, 0));
        s03.setPreferredSize(new java.awt.Dimension(48, 48));
        s03.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s03MouseClicked(evt);
            }
        });

        jPanel1.add(s03, new java.awt.GridBagConstraints());

        s04.setBackground(new java.awt.Color(255, 255, 255));
        s04.setForeground(new java.awt.Color(255, 255, 255));
        s04.setEnabled(false);
        s04.setPreferredSize(new java.awt.Dimension(48, 48));
        jPanel1.add(s04, new java.awt.GridBagConstraints());

        s05.setBackground(new java.awt.Color(0, 0, 0));
        s05.setPreferredSize(new java.awt.Dimension(48, 48));
        s05.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s05MouseClicked(evt);
            }
        });

        jPanel1.add(s05, new java.awt.GridBagConstraints());

        s06.setBackground(new java.awt.Color(255, 255, 255));
        s06.setForeground(new java.awt.Color(255, 255, 255));
        s06.setEnabled(false);
        s06.setPreferredSize(new java.awt.Dimension(48, 48));
        jPanel1.add(s06, new java.awt.GridBagConstraints());

        s07.setBackground(new java.awt.Color(0, 0, 0));
        s07.setPreferredSize(new java.awt.Dimension(48, 48));
        s07.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s07MouseClicked(evt);
            }
        });

        jPanel1.add(s07, new java.awt.GridBagConstraints());

        s12.setBackground(new java.awt.Color(0, 0, 0));
        s12.setPreferredSize(new java.awt.Dimension(48, 48));
        s12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s12MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        jPanel1.add(s12, gridBagConstraints);

        s13.setBackground(new java.awt.Color(255, 255, 255));
        s13.setForeground(new java.awt.Color(255, 255, 255));
        s13.setEnabled(false);
        s13.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        jPanel1.add(s13, gridBagConstraints);

        s14.setBackground(new java.awt.Color(0, 0, 0));
        s14.setPreferredSize(new java.awt.Dimension(48, 48));
        s14.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s14MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        jPanel1.add(s14, gridBagConstraints);

        s15.setBackground(new java.awt.Color(255, 255, 255));
        s15.setForeground(new java.awt.Color(255, 255, 255));
        s15.setEnabled(false);
        s15.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 1;
        jPanel1.add(s15, gridBagConstraints);

        s16.setBackground(new java.awt.Color(0, 0, 0));
        s16.setPreferredSize(new java.awt.Dimension(48, 48));
        s16.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s16MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 1;
        jPanel1.add(s16, gridBagConstraints);

        s17.setBackground(new java.awt.Color(255, 255, 255));
        s17.setForeground(new java.awt.Color(255, 255, 255));
        s17.setEnabled(false);
        s17.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 1;
        jPanel1.add(s17, gridBagConstraints);

        s70.setBackground(new java.awt.Color(0, 0, 0));
        s70.setPreferredSize(new java.awt.Dimension(48, 48));
        s70.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s70MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        jPanel1.add(s70, gridBagConstraints);

        s71.setBackground(new java.awt.Color(255, 255, 255));
        s71.setForeground(new java.awt.Color(255, 255, 255));
        s71.setEnabled(false);
        s71.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        jPanel1.add(s71, gridBagConstraints);

        s50.setBackground(new java.awt.Color(0, 0, 0));
        s50.setPreferredSize(new java.awt.Dimension(48, 48));
        s50.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s50MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        jPanel1.add(s50, gridBagConstraints);

        s51.setBackground(new java.awt.Color(255, 255, 255));
        s51.setForeground(new java.awt.Color(255, 255, 255));
        s51.setEnabled(false);
        s51.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        jPanel1.add(s51, gridBagConstraints);

        s40.setBackground(new java.awt.Color(255, 255, 255));
        s40.setForeground(new java.awt.Color(255, 255, 255));
        s40.setEnabled(false);
        s40.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel1.add(s40, gridBagConstraints);

        s41.setBackground(new java.awt.Color(0, 0, 0));
        s41.setPreferredSize(new java.awt.Dimension(48, 48));
        s41.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s41MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        jPanel1.add(s41, gridBagConstraints);

        s30.setBackground(new java.awt.Color(0, 0, 0));
        s30.setPreferredSize(new java.awt.Dimension(48, 48));
        s30.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s30MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        jPanel1.add(s30, gridBagConstraints);

        s31.setBackground(new java.awt.Color(255, 255, 255));
        s31.setForeground(new java.awt.Color(255, 255, 255));
        s31.setEnabled(false);
        s31.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        jPanel1.add(s31, gridBagConstraints);

        s20.setBackground(new java.awt.Color(255, 255, 255));
        s20.setForeground(new java.awt.Color(255, 255, 255));
        s20.setEnabled(false);
        s20.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel1.add(s20, gridBagConstraints);

        s21.setBackground(new java.awt.Color(0, 0, 0));
        s21.setPreferredSize(new java.awt.Dimension(48, 48));
        s21.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s21MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        jPanel1.add(s21, gridBagConstraints);

        s60.setBackground(new java.awt.Color(255, 255, 255));
        s60.setForeground(new java.awt.Color(255, 255, 255));
        s60.setEnabled(false);
        s60.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        jPanel1.add(s60, gridBagConstraints);

        s61.setBackground(new java.awt.Color(0, 0, 0));
        s61.setPreferredSize(new java.awt.Dimension(48, 48));
        s61.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s61MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        jPanel1.add(s61, gridBagConstraints);

        s42.setBackground(new java.awt.Color(255, 255, 255));
        s42.setForeground(new java.awt.Color(255, 255, 255));
        s42.setEnabled(false);
        s42.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        jPanel1.add(s42, gridBagConstraints);

        s43.setBackground(new java.awt.Color(0, 0, 0));
        s43.setPreferredSize(new java.awt.Dimension(48, 48));
        s43.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s43MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 4;
        jPanel1.add(s43, gridBagConstraints);

        s44.setBackground(new java.awt.Color(255, 255, 255));
        s44.setForeground(new java.awt.Color(255, 255, 255));
        s44.setEnabled(false);
        s44.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        jPanel1.add(s44, gridBagConstraints);

        s32.setBackground(new java.awt.Color(0, 0, 0));
        s32.setPreferredSize(new java.awt.Dimension(48, 48));
        s32.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s32MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        jPanel1.add(s32, gridBagConstraints);

        s33.setBackground(new java.awt.Color(255, 255, 255));
        s33.setForeground(new java.awt.Color(255, 255, 255));
        s33.setEnabled(false);
        s33.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 3;
        jPanel1.add(s33, gridBagConstraints);

        s34.setBackground(new java.awt.Color(0, 0, 0));
        s34.setPreferredSize(new java.awt.Dimension(48, 48));
        s34.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s34MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 3;
        jPanel1.add(s34, gridBagConstraints);

        s22.setBackground(new java.awt.Color(255, 255, 255));
        s22.setForeground(new java.awt.Color(255, 255, 255));
        s22.setEnabled(false);
        s22.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel1.add(s22, gridBagConstraints);

        s23.setBackground(new java.awt.Color(0, 0, 0));
        s23.setPreferredSize(new java.awt.Dimension(48, 48));
        s23.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s23MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        jPanel1.add(s23, gridBagConstraints);

        s24.setBackground(new java.awt.Color(255, 255, 255));
        s24.setForeground(new java.awt.Color(255, 255, 255));
        s24.setEnabled(false);
        s24.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        jPanel1.add(s24, gridBagConstraints);

        s62.setBackground(new java.awt.Color(255, 255, 255));
        s62.setForeground(new java.awt.Color(255, 255, 255));
        s62.setEnabled(false);
        s62.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        jPanel1.add(s62, gridBagConstraints);

        s63.setBackground(new java.awt.Color(0, 0, 0));
        s63.setPreferredSize(new java.awt.Dimension(48, 48));
        s63.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s63MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 6;
        jPanel1.add(s63, gridBagConstraints);

        s52.setBackground(new java.awt.Color(0, 0, 0));
        s52.setPreferredSize(new java.awt.Dimension(48, 48));
        s52.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s52MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 5;
        jPanel1.add(s52, gridBagConstraints);

        s53.setBackground(new java.awt.Color(255, 255, 255));
        s53.setForeground(new java.awt.Color(255, 255, 255));
        s53.setEnabled(false);
        s53.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 5;
        jPanel1.add(s53, gridBagConstraints);

        s72.setBackground(new java.awt.Color(0, 0, 0));
        s72.setPreferredSize(new java.awt.Dimension(48, 48));
        s72.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s72MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 7;
        jPanel1.add(s72, gridBagConstraints);

        s73.setBackground(new java.awt.Color(255, 255, 255));
        s73.setForeground(new java.awt.Color(255, 255, 255));
        s73.setEnabled(false);
        s73.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 7;
        jPanel1.add(s73, gridBagConstraints);

        s74.setBackground(new java.awt.Color(0, 0, 0));
        s74.setPreferredSize(new java.awt.Dimension(48, 48));
        s74.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s74MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 7;
        jPanel1.add(s74, gridBagConstraints);

        s64.setBackground(new java.awt.Color(255, 255, 255));
        s64.setForeground(new java.awt.Color(255, 255, 255));
        s64.setEnabled(false);
        s64.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 6;
        jPanel1.add(s64, gridBagConstraints);

        s54.setBackground(new java.awt.Color(0, 0, 0));
        s54.setPreferredSize(new java.awt.Dimension(48, 48));
        s54.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s54MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 5;
        jPanel1.add(s54, gridBagConstraints);

        s25.setBackground(new java.awt.Color(0, 0, 0));
        s25.setPreferredSize(new java.awt.Dimension(48, 48));
        s25.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s25MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 2;
        jPanel1.add(s25, gridBagConstraints);

        s35.setBackground(new java.awt.Color(255, 255, 255));
        s35.setForeground(new java.awt.Color(255, 255, 255));
        s35.setEnabled(false);
        s35.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 3;
        jPanel1.add(s35, gridBagConstraints);

        s45.setBackground(new java.awt.Color(0, 0, 0));
        s45.setPreferredSize(new java.awt.Dimension(48, 48));
        s45.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s45MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 4;
        jPanel1.add(s45, gridBagConstraints);

        s55.setBackground(new java.awt.Color(255, 255, 255));
        s55.setForeground(new java.awt.Color(255, 255, 255));
        s55.setEnabled(false);
        s55.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 5;
        jPanel1.add(s55, gridBagConstraints);

        s65.setBackground(new java.awt.Color(0, 0, 0));
        s65.setPreferredSize(new java.awt.Dimension(48, 48));
        s65.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s65MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 6;
        jPanel1.add(s65, gridBagConstraints);

        s75.setBackground(new java.awt.Color(255, 255, 255));
        s75.setForeground(new java.awt.Color(255, 255, 255));
        s75.setEnabled(false);
        s75.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 7;
        jPanel1.add(s75, gridBagConstraints);

        s26.setBackground(new java.awt.Color(255, 255, 255));
        s26.setForeground(new java.awt.Color(255, 255, 255));
        s26.setEnabled(false);
        s26.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 2;
        jPanel1.add(s26, gridBagConstraints);

        s27.setBackground(new java.awt.Color(0, 0, 0));
        s27.setPreferredSize(new java.awt.Dimension(48, 48));
        s27.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s27MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 2;
        jPanel1.add(s27, gridBagConstraints);

        s36.setBackground(new java.awt.Color(0, 0, 0));
        s36.setPreferredSize(new java.awt.Dimension(48, 48));
        s36.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s36MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 3;
        jPanel1.add(s36, gridBagConstraints);

        s37.setBackground(new java.awt.Color(255, 255, 255));
        s37.setForeground(new java.awt.Color(255, 255, 255));
        s37.setEnabled(false);
        s37.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 3;
        jPanel1.add(s37, gridBagConstraints);

        s46.setBackground(new java.awt.Color(255, 255, 255));
        s46.setForeground(new java.awt.Color(255, 255, 255));
        s46.setEnabled(false);
        s46.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 4;
        jPanel1.add(s46, gridBagConstraints);

        s47.setBackground(new java.awt.Color(0, 0, 0));
        s47.setPreferredSize(new java.awt.Dimension(48, 48));
        s47.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s47MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 4;
        jPanel1.add(s47, gridBagConstraints);

        s76.setBackground(new java.awt.Color(0, 0, 0));
        s76.setPreferredSize(new java.awt.Dimension(48, 48));
        s76.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s76MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 7;
        jPanel1.add(s76, gridBagConstraints);

        s66.setBackground(new java.awt.Color(255, 255, 255));
        s66.setForeground(new java.awt.Color(255, 255, 255));
        s66.setEnabled(false);
        s66.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 6;
        jPanel1.add(s66, gridBagConstraints);

        s56.setBackground(new java.awt.Color(0, 0, 0));
        s56.setPreferredSize(new java.awt.Dimension(48, 48));
        s56.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s56MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 5;
        jPanel1.add(s56, gridBagConstraints);

        s67.setBackground(new java.awt.Color(0, 0, 0));
        s67.setPreferredSize(new java.awt.Dimension(48, 48));
        s67.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                s67MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 6;
        jPanel1.add(s67, gridBagConstraints);

        s57.setBackground(new java.awt.Color(255, 255, 255));
        s57.setForeground(new java.awt.Color(255, 255, 255));
        s57.setEnabled(false);
        s57.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 5;
        jPanel1.add(s57, gridBagConstraints);

        s77.setBackground(new java.awt.Color(255, 255, 255));
        s77.setForeground(new java.awt.Color(255, 255, 255));
        s77.setEnabled(false);
        s77.setPreferredSize(new java.awt.Dimension(48, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 7;
        jPanel1.add(s77, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridheight = 14;
        getContentPane().add(jPanel1, gridBagConstraints);

        my_icon.setIcon(new javax.swing.ImageIcon("C:\\Documents and Settings\\Austin\\My Documents\\Checkers\\src\\r_24.JPG"));
        my_icon.setText("jLabel1");
        my_icon.setMaximumSize(new java.awt.Dimension(24, 24));
        my_icon.setMinimumSize(new java.awt.Dimension(24, 24));
        my_icon.setPreferredSize(new java.awt.Dimension(24, 24));
        getContentPane().add(my_icon, new java.awt.GridBagConstraints());

        my_label.setText("jLabel2");
        my_label.setMaximumSize(new java.awt.Dimension(200, 24));
        my_label.setMinimumSize(new java.awt.Dimension(200, 24));
        my_label.setPreferredSize(new java.awt.Dimension(200, 24));
        getContentPane().add(my_label, new java.awt.GridBagConstraints());

        opponent_label.setText("jLabel2");
        opponent_label.setMaximumSize(new java.awt.Dimension(200, 24));
        opponent_label.setMinimumSize(new java.awt.Dimension(200, 24));
        opponent_label.setPreferredSize(new java.awt.Dimension(200, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        getContentPane().add(opponent_label, gridBagConstraints);

        opponent_icon.setIcon(new javax.swing.ImageIcon("C:\\Documents and Settings\\Austin\\My Documents\\Checkers\\src\\b_24.JPG"));
        opponent_icon.setText("jLabel1");
        opponent_icon.setMaximumSize(new java.awt.Dimension(24, 24));
        opponent_icon.setMinimumSize(new java.awt.Dimension(24, 24));
        opponent_icon.setPreferredSize(new java.awt.Dimension(24, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        getContentPane().add(opponent_icon, gridBagConstraints);

        turn_icon.setIcon(new javax.swing.ImageIcon("C:\\Documents and Settings\\Austin\\My Documents\\Checkers\\src\\b_24.JPG"));
        turn_icon.setText("jLabel1");
        turn_icon.setMaximumSize(new java.awt.Dimension(24, 24));
        turn_icon.setMinimumSize(new java.awt.Dimension(24, 24));
        turn_icon.setPreferredSize(new java.awt.Dimension(24, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 13;
        getContentPane().add(turn_icon, gridBagConstraints);

        turn_label.setText("jLabel2");
        turn_label.setMaximumSize(new java.awt.Dimension(200, 24));
        turn_label.setMinimumSize(new java.awt.Dimension(200, 24));
        turn_label.setPreferredSize(new java.awt.Dimension(200, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 13;
        getContentPane().add(turn_label, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void s76MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s76MouseClicked
        myJump.moveSpace(7,6);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
        
    }//GEN-LAST:event_s76MouseClicked
    
    private void s74MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s74MouseClicked
        myJump.moveSpace(7,4);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s74MouseClicked
    
    private void s72MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s72MouseClicked
        myJump.moveSpace(7,2);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s72MouseClicked
    
    private void s70MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s70MouseClicked
        myJump.moveSpace(7,0);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s70MouseClicked
    
    private void s67MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s67MouseClicked
        myJump.moveSpace(6,7);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s67MouseClicked
    
    private void s65MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s65MouseClicked
        myJump.moveSpace(6,5);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s65MouseClicked
    
    private void s63MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s63MouseClicked
        myJump.moveSpace(6,3);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s63MouseClicked
    
    private void s61MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s61MouseClicked
        myJump.moveSpace(6,1);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s61MouseClicked
    
    private void s56MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s56MouseClicked
        myJump.moveSpace(5,6);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s56MouseClicked
    
    private void s54MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s54MouseClicked
        myJump.moveSpace(5,4);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s54MouseClicked
    
    private void s52MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s52MouseClicked
        myJump.moveSpace(5,2);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s52MouseClicked
    
    private void s50MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s50MouseClicked
        myJump.moveSpace(5,0);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s50MouseClicked
    
    private void s47MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s47MouseClicked
        myJump.moveSpace(4,7);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s47MouseClicked
    
    private void s45MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s45MouseClicked
        myJump.moveSpace(4,5);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s45MouseClicked
    
    private void s43MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s43MouseClicked
        myJump.moveSpace(4,3);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s43MouseClicked
    
    private void s41MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s41MouseClicked
        myJump.moveSpace(4,1);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s41MouseClicked
    
    private void s36MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s36MouseClicked
        myJump.moveSpace(3,6);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s36MouseClicked
    
    private void s34MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s34MouseClicked
        myJump.moveSpace(3,4);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s34MouseClicked
    
    private void s32MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s32MouseClicked
        myJump.moveSpace(3,2);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s32MouseClicked
    
    private void s30MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s30MouseClicked
        myJump.moveSpace(3,0);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s30MouseClicked
    
    private void s27MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s27MouseClicked
        myJump.moveSpace(2,7);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s27MouseClicked
    
    private void s25MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s25MouseClicked
        myJump.moveSpace(2,5);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s25MouseClicked
    
    private void s23MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s23MouseClicked
        myJump.moveSpace(2,3);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s23MouseClicked
    
    private void s21MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s21MouseClicked
        myJump.moveSpace(2,1);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s21MouseClicked
    
    private void s16MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s16MouseClicked
// TODO add your handling code here:
        System.out.println("moving");
        myJump.moveSpace(1,6);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s16MouseClicked
    
    private void s14MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s14MouseClicked
// TODO add your handling code here:
        System.out.println("moving");
        myJump.moveSpace(1,4);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s14MouseClicked
    
    private void s10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s10MouseClicked
// TODO add your handling code here:
        System.out.println("moving");
        myJump.moveSpace(1,0);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s10MouseClicked
    
    private void s07MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s07MouseClicked
// TODO add your handling code here:
        System.out.println("moving");
        myJump.moveSpace(0,7);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s07MouseClicked
    
    private void s05MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s05MouseClicked
// TODO add your handling code here:
        System.out.println("moving");
        myJump.moveSpace(0,5);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s05MouseClicked
    
    private void s03MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s03MouseClicked
// TODO add your handling code here:
        System.out.println("moving");
        myJump.moveSpace(0,3);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s03MouseClicked
    
    private void s12MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s12MouseClicked
// TODO add your handling code here:
        System.out.println("moving");
        myJump.moveSpace(1,2);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s12MouseClicked
    
    private void s01MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_s01MouseClicked
// TODO add your handling code here:
        System.out.println("moving");
        myJump.moveSpace(0,1);
        
        if(myJump.hasJumped()) {
            //Send Jump
            theGame.sendString("m" + myJump.getX1() + myJump.getY1() + myJump.getX2() + myJump.getY2());
        }
    }//GEN-LAST:event_s01MouseClicked
        
    
    public void edit_square(String myChange) {
        //myChange.
    }
    
    public void setMyTurn(boolean turnVal) {
        if(turnVal) {
            this.turn_label.setText(myName);
            this.turn_icon.setIcon(new javax.swing.ImageIcon(myIcon));
        } else {
            this.turn_label.setText(opName);
            this.turn_icon.setIcon(new javax.swing.ImageIcon(opIcon));
        }
    }
    
    /**
     * enables and disables wether or not the user can edit the board
     *
     * @param editVal if the player can edit it
     **/
    public void editBoard(boolean editVal) {
        int i,j;
        for(i=0;i<8;i++) {
            for(j=0;j<8;j++) {
                if(((i + j) % 2) != 0) {
                    if(editVal) {
                        String buttonLabel = myBoard[i][j].getText();
                        String myKingS = Character.toString(myKing);
                        String myPieceS = Character.toString(myPiece);
                        if(buttonLabel.equals(myKingS) || buttonLabel.equals(myPieceS)) {
                            myBoard[i][j].setEnabled(true);
                        }
                    } else {
                        myBoard[i][j].setEnabled(false);
                    }
                }
            }
        }
    }
    
    /**
     * Alows the player to edit the board
     **/
    public void startTurn() {
        editBoard(true);
        setMyTurn(true);
        myJump = new Jump();
    }
    
    
    /**
     * Disables the player's ability to edit the board
     **/
    public void waitTurn() {
        editBoard(false);
        setMyTurn(false);
        
    }
    
    /**
     * Changes the pieces on the board specified by the transport stream
     *
     * @param pieceArray a String of chars representing the pieces to be changed
     *      Format (each a char): <type><xloc><yloc><type><xloc><yloc>......
     */
    public void changePieces(String pieceArray) {
        int i = 0;
        char symbol;
        char x;
        char y;
        while(i < pieceArray.length()) {
            symbol = pieceArray.charAt(i);
            x = pieceArray.charAt(i+1);
            y = pieceArray.charAt(i+2);
            
            if(symbol == '_') {
                myBoard[x][y].setIcon(null);
                
            } else if(symbol == 'r') {
                myBoard[x][y].setIcon(new javax.swing.ImageIcon(red));
                //myBoard[i][j].setIcon(new javax.swing.ImageIcon(opponentsIcon));
                
            } else if(symbol == 'R') {
                myBoard[x][y].setIcon(new javax.swing.ImageIcon(red_k));
            } else if(symbol == 'b') {
                myBoard[x][y].setIcon(new javax.swing.ImageIcon(black));
            } else if(symbol == 'B') {
                myBoard[x][y].setIcon(new javax.swing.ImageIcon(black_k));
            }
            i = i + 3;
        }
    }
    
    
    /**
     * Notifies the player they have won the game.
     */
    public void win() {
        System.out.println("You have won the game!");
    }
    
    /**
     * Notifies the player they have lost the game
     */
    public void lose() {
        System.out.println("You have lost the game.");
    }
    
    /*
     * Notifies the player an error has occured
     */
    public void error(String myError) {
        System.out.println(myError);
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ClientGUI Me = new ClientGUI("Austin", "Kingdon", "Austin-Kingdon", 'b');
                Me.setVisible(true);
                
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel my_icon;
    private javax.swing.JLabel my_label;
    private javax.swing.JLabel opponent_icon;
    private javax.swing.JLabel opponent_label;
    private javax.swing.JButton s00;
    private javax.swing.JButton s01;
    private javax.swing.JButton s02;
    private javax.swing.JButton s03;
    private javax.swing.JButton s04;
    private javax.swing.JButton s05;
    private javax.swing.JButton s06;
    private javax.swing.JButton s07;
    private javax.swing.JButton s10;
    private javax.swing.JButton s11;
    private javax.swing.JButton s12;
    private javax.swing.JButton s13;
    private javax.swing.JButton s14;
    private javax.swing.JButton s15;
    private javax.swing.JButton s16;
    private javax.swing.JButton s17;
    private javax.swing.JButton s20;
    private javax.swing.JButton s21;
    private javax.swing.JButton s22;
    private javax.swing.JButton s23;
    private javax.swing.JButton s24;
    private javax.swing.JButton s25;
    private javax.swing.JButton s26;
    private javax.swing.JButton s27;
    private javax.swing.JButton s30;
    private javax.swing.JButton s31;
    private javax.swing.JButton s32;
    private javax.swing.JButton s33;
    private javax.swing.JButton s34;
    private javax.swing.JButton s35;
    private javax.swing.JButton s36;
    private javax.swing.JButton s37;
    private javax.swing.JButton s40;
    private javax.swing.JButton s41;
    private javax.swing.JButton s42;
    private javax.swing.JButton s43;
    private javax.swing.JButton s44;
    private javax.swing.JButton s45;
    private javax.swing.JButton s46;
    private javax.swing.JButton s47;
    private javax.swing.JButton s50;
    private javax.swing.JButton s51;
    private javax.swing.JButton s52;
    private javax.swing.JButton s53;
    private javax.swing.JButton s54;
    private javax.swing.JButton s55;
    private javax.swing.JButton s56;
    private javax.swing.JButton s57;
    private javax.swing.JButton s60;
    private javax.swing.JButton s61;
    private javax.swing.JButton s62;
    private javax.swing.JButton s63;
    private javax.swing.JButton s64;
    private javax.swing.JButton s65;
    private javax.swing.JButton s66;
    private javax.swing.JButton s67;
    private javax.swing.JButton s70;
    private javax.swing.JButton s71;
    private javax.swing.JButton s72;
    private javax.swing.JButton s73;
    private javax.swing.JButton s74;
    private javax.swing.JButton s75;
    private javax.swing.JButton s76;
    private javax.swing.JButton s77;
    private javax.swing.JLabel turn_icon;
    private javax.swing.JLabel turn_label;
    // End of variables declaration//GEN-END:variables
    
}
