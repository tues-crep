import java.util.*;
import java.lang.*;
import java.io.*;
import java.net.*;

/*
 * Board.java
 *
 * Created on January 13, 2007, 8:45 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author Austin
 */
public class Board {
    
    public char[][] field;
    
    private String player1Name;
    
    private String player2Name;
    
    private String boardName;
    
    private int player1PortNumber;
    
    private int player2PortNumber;
    
    private String player1Address;
    
    private String player2Address;

    public char GetPosition(int x, int y) {
	    return field[x][y];
    }
   public void SetPosition(int x, int y, char val) {
	   field[x][y] = val;
   }


    /** Creates a new instance of Board */
    public Board() {
	    int i, j;
	    field = new char[8][8];
        
        for(i = 0;i < 8;i++) {
		for(j = 0;j<3;j++) {
		    if(((i+j)%2) == 0) {
			    field[i][j] = 'r';
		    }
		    else {
		    	field[i][j] = ' ';
		    }
		}
		
		for(j = 3; j<5; j++){
			if(((i+j)%2) == 0) {
			    field[i][j] = '\0';
		    }
		    else {
		    	field[i][j] = ' ';
		    }
		}

		for(j = 5;j<8;j++) {
			if(((i+j)%2) == 0) {
                field[i][j] = 'b';
			}
			else {
				field[i][j] = ' ';
			}
		}
	}

    }

    public String toString() {
    	String ret="";
    	for(int x=8; x>=0; x--){
    		
    		if(x==8) {
    			ret += "  0 1 2 3 4 5 6 7";
    		}
    		else {
    			ret+=x + " ";
    			
    			for(int y=0; y<8; y++){
        			ret += field[y][x];
        			ret += ' ';
        		}	
    		}
    		
    		ret+='\n';
    		
    	}
		return ret;
    }
    
    public char charAt(int x, int y) {
        return field[x][y];
    }
    
}
