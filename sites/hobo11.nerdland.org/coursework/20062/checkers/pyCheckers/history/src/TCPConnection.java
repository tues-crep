import java.util.*;
import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.concurrent.*;

/*
 * TCPConnection.java
 *
 *
 * Tos change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author Austin
 */
public class TCPConnection extends BaseConnection {
    
    private Socket socket;
    private BufferedReader reader;
    private DataOutputStream os;
    
    private int port;
    private String host;
    private String protocol;
    
    
    public void run() {
        //Run the ServerGame
    }
    
    public TCPConnection(String host, int port) {
        System.out.println("Init TCPConnection from host and port.");
        try {
            this.host = host;
            this.port = port;
            this.protocol = "TCP";
            this.socket = new Socket(host, port);
            this.os = new DataOutputStream(this.socket.getOutputStream());
            this.reader = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
        } catch(java.net.UnknownHostException e) {
            System.out.println("The host '" + host + "' does not exist or is inaccessable.");
        } catch (java.io.IOException e) {
            System.out.println("Another error occured.");
        }
    }
    
    /** Creates a new instance of Player */
    public TCPConnection(Socket mySocket) {
        //board = new Board();
        //this.gameName = gameName;
        this.socket = mySocket;
        //this.player1Name = player1Name;
        //this.player2Name = player2Name;
        
        try {
            this.reader = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            this.os = new DataOutputStream(this.socket.getOutputStream());
        } catch (Exception e) {
            System.out.println("E:" + e.getMessage());
        }
        
        int i;
        
        System.out.println("Init TCPConnection from socket");
        
        
    }
    
    public boolean isConnected() {
        return socket.isConnected();
    }
    
    
    public void sendData(String myData) {
        System.out.println("Sending Data: " + myData);
        try {
            this.os.writeBytes(myData + "\n");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    
    public String rcvData() {
        String inString = "";
        //System.out.println("Waiting for Data");
        try {
            inString = reader.readLine();
            //System.out.println("Received Data");
            return inString;
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
        return "";
        
    }
    
    public void disconnect() {
        try {
            socket.close();
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    
    
    
    public static void main(String args[]) {
        
        
        
    }
    
}
