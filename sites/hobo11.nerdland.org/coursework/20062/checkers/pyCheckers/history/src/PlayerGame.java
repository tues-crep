import java.util.*;
import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.concurrent.*;

/*
 * PlayerGame.java
 *
 * The PlayerGame class
 */

/**
 *
 * @author Austin
 */
public class PlayerGame implements Runnable {
    
    private boolean myTurn; // Is it my turn?
    
    private ClientGUI myGUI; // The player's GUI
    
    private char myColor; // PLayer's color
    
    private boolean gameRunning; // Is the game still running?
    
    private String yourName; // Player's name
    
    private String otherName; // Opponent's name
    
    private String gameName; // Game Name
    
    BaseConnection myConnection; //Player's connection
    /**
     * PlayerGame Default Constructor
     */
    public PlayerGame() {
    }
    
    /**
     * PlayerGame constructor
     *
     * @param yourName Player's name
     * @param otherName Opponent's name
     * @param gameName the Game's Name
     * @param myConnectoin Player's connection
     **/
    public PlayerGame(String yourName, String otherName, String gameName, BaseConnection myConnection) {
        this.yourName = yourName;
        this.otherName = otherName;
        this.gameName = gameName;
        this.myConnection = myConnection;
    }
    
    public void sendString(String data) {
        if(myTurn) {
            myConnection.sendData(data);
        }
    }
    /**
     * Thread that starts the game
     */
    public void run() {
        
        System.out.println("PlayerGame started");
        
        // Determine player's color'
        if(gameName.startsWith(yourName)) {
            myColor = 'r';
        } else {
            myColor = 'b';
        }
        
        // Set game to running
        gameRunning = true;
        
        //Create GUI
        myGUI = new ClientGUI(yourName, otherName, gameName, myColor, this);
        myGUI.setVisible(true);
        
        String stream;
        char command;
        int i;
        
        // While the game is running
        while(gameRunning && myConnection.isConnected()) {
            
            // Receive and parse data
            
            stream = myConnection.rcvData();
                        System.out.println(stream);
            command = stream.charAt(0);
            stream = stream.substring(1);
            System.out.println("Stream:" + stream);

            // If type is go, start turn
            if(command == 'g') {
                        System.out.println("go");
                myGUI.startTurn();
                myTurn = true;
                
                // If type is wait, wait for other player/server response
            } else if(command == 'w') {
                                        System.out.println("wait");
                myGUI.waitTurn();
		myGUI.editBoard(false);
                myTurn = false;
                
                // If type is move, update the board
            } else if(command == 'm') {
                                        System.out.println("move");
                myGUI.changePieces(stream);
                
                // If type is game won, notify the player and exit loop
            } else if(command == 't') {
                                        System.out.println("win");
                gameRunning = false;
                myGUI.win();
                
                // If type is game lost, notify the player and exit loop
            } else if(command == 'l') {
                                        System.out.println("lose");
                gameRunning = false;
                myGUI.lose();
                
                // If type is game error, notify player and exit loop
            } else if(command == 'e') {
                                        System.out.println("error");
                gameRunning = false;
                stream = stream.substring(2);
                System.out.println(stream);
                myGUI.error(stream);
            }
        }
        
        //Disconnect after game
        // Exit GUI
        myConnection.disconnect();
    }
    
}
