import java.util.*;
import java.lang.*;
import java.io.*;
import java.net.*;

/*
 * Jump.java
 *
 
 */

/**
 *
 * @author Austin
 */
public class Jump {
    
    int x1 = 8;
    
    int x2 = 8;
    
    int y1 = 8;
    
    int y2 = 8;
    
    int jumpCount;
    
    public void moveSpace(int x, int y) {
        x1 = x2;
        y1 = y2;
        x2 = x;
        y2 = y;
        jumpCount++;
        System.out.println("JumpCount: " + jumpCount);
    }
    
    public boolean hasJumped() {
        if(jumpCount >= 2) {
            return true;
        } else {
            return false;
        }
    }
    
    public int getX1() {
        return x1;
    }
    
    public int getX2() {
        return x2;
    }
    
    public int getY1() {
        return y1;
    }
    
    public int getY2() {
        return y2;
    }
    
    /** Creates a new instance of Board */
    public Jump() {
        jumpCount = 0;
        //x1 = x;
        //x2 = x;
        //y1 = y;
        //y2 = y;
    }
    
}
