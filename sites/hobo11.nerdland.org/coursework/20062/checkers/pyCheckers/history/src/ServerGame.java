import java.util.*;
import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.concurrent.*;

/*
 * ServerGame.java
 *
 * An instance of a game playing on the server
 */

/**
 *
 * @author Austin
 */
public class ServerGame implements Runnable {
    
    private boolean gameRunning; // Determines if the game has ended
    
    private int turn; // Current player's turn
    
    private BaseConnection p1Connection; // First player's connection
    
    private BaseConnection p2Connection; // Second player's connection
    
    private Board board; // The play board
    
    private String gameName; // The game name
    
    private Checkers myCheckers; // Game logic
    
    private String player1Name; // First player's name
    
    private String player2Name; // Second player's name
    
    /**
     * Gets the name of the game
     **/
    public String getGameName() {
        return gameName;
    }
    
    
    /**
     * Starts the game as a thread
     **/
    public void run() {
        myCheckers = new Checkers(board);
        System.out.println("StartGame");
        gameRunning = true; // Sets the game to running
        
        // Sends connected commands and notifies p1 to start and p2 to wait
        p1Connection.sendData("c");
        p2Connection.sendData("c");
        p2Connection.sendData("w");
        p1Connection.sendData("g");
        turn = 1; // Player 1's turn'
        
        String stream = null;
        String sendString;
        char command;
        char myChar = '\0';
        int i;
        int x1, y1, x2, y2;
        
        while(gameRunning && p1Connection.isConnected() && p2Connection.isConnected()) {
            // If player 1's turn'
            if(turn == 1) {
                stream = p1Connection.rcvData();
            } else {
                stream = p2Connection.rcvData();
            }
            
            System.out.println("p1: " + stream);
            
            command = stream.charAt(0);
            stream = stream.substring(1);
            
            
            
            if(command == 'm') {
                
                x1 = Integer.parseInt(stream.substring(0,1));
                y1 = Integer.parseInt(stream.substring(1,2));
                x2 = Integer.parseInt(stream.substring(2,3));
                y2 = Integer.parseInt(stream.substring(3,4));
                
                if(myCheckers.pieceAt(x1, y1) == 'r' || myCheckers.pieceAt(x1, y1) == 'R') {
                    myChar = 'r';
                } else if(myCheckers.pieceAt(x1, y1) == 'b' || myCheckers.pieceAt(x1, y1) == 'B') {
                    myChar = 'b';
                }
                
                if(myCheckers.isValidDirection(x1, y1, x2, y2)) {
                    System.out.println("IsValidDirection");
                    
                    if(myCheckers.jumpAvailable(myChar)) {
                        System.out.println("JumpIsAvailable");
                        if(myCheckers.isJumpingMove(x1, y1, x2, y2)) {
                            System.out.println("IsJumpingMove");
                            
                            // Move piece and get string to send
                            sendString = myCheckers.move(x1, y1, x2, y2);
                            // Label string as a move
                            sendString = "m" + sendString;
                            //send to players the jump
                            p1Connection.sendData(sendString);
                            p2Connection.sendData(sendString);
                            
                            // check of more jumps can be done
                            if(myCheckers.isPieceJumpAvailable(x2, y2)) {
                                System.out.println("JumpIsAvailable");
                                // If yes, continue jump
                                // FIXME Nothing needs to be done?
                            } else {
                                // Otherwise, switch turns
                                if(turn == 1) {
                                    p1Connection.sendData("w");
                                    p2Connection.sendData("g");
                                } else {
                                    p2Connection.sendData("w");
                                    p1Connection.sendData("g");
                                    
                                }
                            }
                            
                        } else {
                            if(turn == 1) {
                                p1Connection.sendData("iYou have a jump you can make.  You must make the jump.");
                            } else {
                                p2Connection.sendData("iYou have a jump you can make.  You must make the jump.");
                            }
                        }
                        
                        
                    } else {
                        if(myCheckers.isSimpleMove(x1, y1, x2, y2)) {
                            System.out.println("IsSimpleMove");
                            // Move piece and get string to send
                            sendString = myCheckers.move(x1, y1, x2, y2);
                            
                            // Label string as a move
                            sendString = "m" + sendString;
                            
                            //send to players the jump
                            p1Connection.sendData(sendString);
                            p2Connection.sendData(sendString);
                            
                            // Switch turns
                            if(turn == 1) {
                                p1Connection.sendData("w");
                                p2Connection.sendData("g");
                            } else {
                                p2Connection.sendData("w");
                                p1Connection.sendData("g");
                                
                            }
                            
                            // send players changes
                            //switch turns
                        } else {
                            p1Connection.sendData("iAn unknown error occured.");
                            p2Connection.sendData("iAn unknown error occured.");
                        }
                    }
                    
                    
                    
                    
                    
                } else {
                    if(turn == 1) {
                        p1Connection.sendData("iA non-king piece cannot move backwards.");
                    } else {
                        p2Connection.sendData("iA non-king cannot move backwards.");
                    }
                }
                
                
                myCheckers.move(x1, y1, x2, y2);
                myCheckers.removePiece(x1, y1);
                myCheckers.swap(x1, y1, x2, y2);
            }
            
            /*} else if(turn ==2) {
                stream = p2Connection.rcvData();
             
            }*/
            
            //start listening/reading stuff and coordinating everything
            
        }
        p1Connection.disconnect();
        p2Connection.disconnect();
    }
    
    
    /**
     * ServerGame default constructor
     */
    public ServerGame() {
    }
    
    
    /**
     * Joins second player to the game
     *
     * @param p2Connection second player's connection
     */
    public void joinGame(BaseConnection p2Connection) {
        this.p2Connection = p2Connection;
    }
    
    
    /**
     *Creates a game based on the initiating connection being TCP-based
     *
     * @param gameName The game name
     * @param player1Name First player's name
     * @param player2Name Second player's name
     * @param p1Connection First player's connection
     */
    public ServerGame(String gameName, String player1Name, String player2Name, BaseConnection p1Connection) {
        this.board = new Board();
        this.gameName = gameName;
        this.player1Name = player1Name;
        this.player2Name = player2Name;
        this.p1Connection = p1Connection;
    }
}
