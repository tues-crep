/*
 * Checkers.java
 *
 * Created on January 14, 2007, 9:14 PM
 *
 * This file contains the logic for a Checkers game.
 */

import java.lang.Math;

/**
 *
 * @author Kingdon
 */
public class Checkers {
    Board myBoard;
    
    int p1Count = 12;
    
    int p2Count = 12;
    
    Checkers() {
    }
    
    Checkers(Board b) {
        myBoard=b;
    }
    
    int avg(int val1, int val2) {
        return (val1+val2)/2;
    }
    
//    /**
//     * Check for feasibility of a move.  In simple terms, a
//     * move is infeasible if "from" is not occupied, or if
//     * "to" is occupied, or if either location is not a valid
//     * position on the board.
//     *
//     * @param x1 horizontal position from
//     * @param y1 vertical position from
//     * @param x2 horizontal position to
//     * @param y2 vertical position to
//     */
//    boolean isFeasible(int x1, int y1, int x2, int y2){
//        boolean ret=true;
//
//        // Sanity check; are the coordinates on the board?
//        if( (0>x1 || x1>7) || (0>y1 || y1>7) || (0>x2 || x2>7) || (0>y2 || y2>7) ) {
//            ret=false;
//        }
//
//        // Moving from/to a gap non-space!  Illegal move, try again.
//        if(myBoard.GetPosition(x1, y1)==' ' || myBoard.GetPosition(x2, y2)==' ') {
//            ret=false;
//        }
//
//        // Moving from an empty space!  Illegal move, try again.
//        if(myBoard.GetPosition(x1, y1)=='\0') {
//            ret=false;
//        }
//
//        // Moving to a non-empty space!  Illegal move, try again.
//        if(myBoard.GetPosition(x2, y2)!='\0') {
//            ret=false;
//        }
//
//        return ret;
//    }
    
    /**
     * Check simply if the move described is to a diagonally
     * adjacent space.
     *
     * @param x1 from
     * @param y1
     * @param x2 to
     * @param y2
     * @return
     */
    boolean isSimpleMove(int x1, int y1, int x2, int y2) {
        boolean ret=false;
        
        if(Math.abs(x1-x2)==1 && Math.abs(y1-y2)==1) {
            ret=true;
        }
        
        return ret;
    }
    
    /**
     * Check if the move described is a (valid) jumping move.
     *
     * @param x1 from
     * @param y1
     * @param x2 to
     * @param y2
     * @return
     */
    boolean isJumpingMove(int x1, int y1, int x2, int y2) {
        boolean ret=false;
        // Valid distance for a jump?
        if(Math.abs(x1-x2)==2 && Math.abs(y1-y2)==2) {
            
            // Examine the piece we're to hop over
            int betweenX=avg(x1,x2);
            int betweenY=avg(y1,y2);
            int jumpedPiece=myBoard.GetPosition(betweenX, betweenY);
            
            // Black can only jump Red
            if(myBoard.GetPosition(x1, y1) == 'b' || myBoard.GetPosition(x1, y1) == 'B') {
                if(jumpedPiece=='r' || jumpedPiece=='R') {
                    ret=true;
                }
            }
            // Red can only jump Blacks
            if(myBoard.GetPosition(x1, y1) == 'r' || myBoard.GetPosition(x1, y1) == 'R') {
                if(jumpedPiece=='b' || jumpedPiece=='B') {
                    ret=true;
                }
            }
        }
        return ret;
    }
    
    /**
     * Check if the player of 'color' is in a position to make a jumping
     * move.  This is important information; based on the rules of Checkers,
     * a player must give priority to a jumping move over a simple move.
     *
     * @param color The player's color to check for jumping moves.
     * @return Is a jumping move available?
     */
    boolean jumpAvailable(char myColor) {
        char myKing;
        
        if(myColor == 'r') {
            myKing = 'R';
        } else {
            myKing = 'B';
        }
        
        boolean retVal = false;
        int i,j;
        
        for(i=0;i<8;i++) {
            for(j=0;j<8;j++) {
                if(pieceAt(i,j) == myColor || pieceAt(i,j) == myKing) {
                    if(isPieceJumpAvailable(i,j)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    boolean isPieceJumpAvailable(int x, int y) {
        char myPiece = pieceAt(x, y);
        
        
        if(x <=5 && y <= 5) {
            char pp = pieceAt(x+1, y+1);
            char pppp = pieceAt(x+2, y+2);
            
            if(myPiece == 'R' || myPiece == 'r' ) {
                
                if(pp == 'b' || pp == 'B') {
                    if(pppp == '\0') {
                        return true;
                    }
                    
                }
                
            } else {
                if(pp == 'r' || pp == 'R') {
                    if(pppp == '\0') {
                        return true;
                    }
                }
            }
            
        } else if(x >= 2 && y >= 2) {
            char nn = pieceAt(x-1, y-1);
            char nnnn = pieceAt(x-2, y-2);
            
            if(myPiece == 'R' || myPiece == 'r' ) {
                
                if(nn == 'b' || nn == 'B') {
                    if(nnnn == '\0') {
                        return true;
                    }
                    
                }
            } else {
                if(nn == 'r' || nn == 'R') {
                    if(nnnn == '\0') {
                        return true;
                    }
                }
            }
            
            
        } else if(x <=5 && y >= 2) {
            char pn = pieceAt(x+1,y-1);
            char ppnn = pieceAt(x+2, y-2);
            
            if(myPiece == 'R' || myPiece == 'r' ) {
                
                if(pn == 'b' || pn == 'B') {
                    if(ppnn == '\0') {
                        return true;
                    }
                    
                }
            } else {
                if(pn == 'r' || pn == 'R') {
                    if(ppnn == '\0') {
                        return true;
                    }
                }
            }
            
            
        } else if(x >= 2 && y <= 5) {
            char np = pieceAt(x-1, y+1);
            char nnpp = pieceAt(x-2, y+2);
            
            
            if(myPiece == 'R' || myPiece == 'r' ) {
                
                if(np == 'b' || np == 'B') {
                    if(nnpp == '\0') {
                        return true;
                    }
                }
                
            } else {
                if(np == 'r' || np == 'R') {
                    if(nnpp == '\0') {
                        return true;
                    }
                }
            }
        }
        
        return false;
    }
    
    
//    /**
//     * Check that a move is valid, according to the pieces on the board
//     * and the rules of Checkers.
//     *
//     * @param x1
//     * @param y1
//     * @param x2
//     * @param y2
//     */
//    boolean isValidMove(int x1, int y1, int x2, int y2) {
//        boolean ret=false;
//
//        // First, the move must be feasible, according to the definition above.
//        if(!isFeasible(x1, y1, x2, y2)) {
//            return ret=false;
//        }
//
//        // Check for legal direction.  Only Kings may move backwards.
//        // Otherwise, red moves must have a +y value, and black moves -y.
//        if(!isValidDirection(x1, y1, x2, y2)) {
//            return ret=false;
//        }
//
//        // Check for a jumping move.  Jumping moves are always valid, if
//        // we've made it this far, NB they require special handling to
//        // remove the captured piece, and an extra check in case of double
//        // jumps.
//        if(isJumpingMove(x1, y1, x2, y2)) {
//            //System.out.println("made a jump");
//            return ret=true;
//        }
//
//        // Check for diagonal adjacency.  If the move is diagonally
//        // adjacent (a simple non-jumping move), and there is a valid
//        // jumping move on the board, the move will fail.
//        if(isSimpleMove(x1, y1, x2, y2)) {
//            char checker = myBoard.GetPosition(x1, y1);
//            char color = Character.toLowerCase(checker);
//
//            if(jumpAvailable()) {
//                return ret=false;
//            } else {
//                return ret=true;
//            }
//        }
//
//        return ret;
//    }
    
    /**
     * Is this move traveling in the right direction?
     *
     * Black pieces must go in -y direction, red in +y,
     * unless the piece is a king (in caps) in which case
     * all bets are off, and any direction is fair game!
     *
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @return boolean type represents that the move
     *   is/isnot valid
     */
    boolean isValidDirection(int x1, int y1, int x2, int y2) {
        
        if(myBoard.GetPosition(x1, y1) == 'B' || myBoard.GetPosition(x1, y1) == 'R') {
            return true;
        } else {
            if( myBoard.GetPosition(x1, y1) == 'r' && x1 < x2 ) {
                return true;
            } else if( myBoard.GetPosition(x1, y1) == 'b' && x1 > x2 ) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Make a move, if it's valid.  Also works for jumping.  Should
     * fail if there is a valid jumping move to play, and the move
     * passed is not a jump.
     *
     * @param x1 from
     * @param y1
     *
     * @param x2 to
     * @param y2
     *
     * @return the character code 'w', 'g', 'f' for wait, go, fail.
     */
    public String move(int x1, int y1, int x2, int y2) {
        int betweenX = 0;
        int betweenY = 0;
        // Next, make sure that the move is valid according to the rules of Checkers.
        
        if(pieceAt(x1, y1) == 'r') {
            if(x2 == 7) {
                kingMe(x1, y1);
                
            }
        } else if(pieceAt(x1, y1) == 'b') {
            if(x2 == 0) {
                kingMe(x1, y1);
            }
        }
        
        
        // Are we about to jump a piece?  Remove it from the board.
        if(isJumpingMove(x1, y1, x2, y2)) {
            betweenX=avg(x1,x2);
            betweenY=avg(y1,y2);
            
            removePiece(betweenX, betweenY);
        }
        
        swap(x1, y1, x2, y2);
        String retVal = pieceString(x1, y1) + pieceString(x2, y2) + pieceString(betweenX, betweenY);
        
        return retVal;
    }
    
    /**
     * We just jumped over x,y and it is time to remove the piece.
     * @param x
     * @param y
     */
    void removePiece(int x, int y) {
        myBoard.SetPosition(x, y, '\0');
    }
    
    public boolean playerWon(char myPiece) {
        int i,j;
        char myKing;
        
        if(myPiece == 'r') {
            myKing = 'R';
        } else {
            myKing = 'B';
        }
        
        int pieceCount = 0;
        
        if(myPiece == 'r') {
            for(i=0;i<8;i++) {
                for(j=0;j<8;j++) {
                    if(pieceAt(i,j) == 'B' || pieceAt(i,j) == 'b') {
                        pieceCount++;
                    }
                }
            }
        } else if(myPiece == 'b') {
            for(i=0;i<8;i++) {
                for(j=0;j<8;j++) {
                    if(pieceAt(i,j) == 'R' || pieceAt(i,j) == 'r') {
                        pieceCount++;
                    }
                }
            }
        }
        
        
        if(pieceCount == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    
    /**
     * Swap the contents of two x,y coordinates on the board.
     * This checks not for a logical move, so be sure to use
     * isValidMove before you call this.
     *
     * @param x1 horizontal position from
     * @param y1 vertical position from
     * @param x2 horizontal position to
     * @param y2 vertical position to
     */
    void swap(int x1, int y1, int x2, int y2){
        char temp=myBoard.GetPosition(x2, y2);
        
        myBoard.SetPosition(x2, y2, myBoard.GetPosition(x1, y1));
        myBoard.SetPosition(x1, y1, temp);
    }
    
    public char pieceAt(int x, int y) {
        return myBoard.charAt(x, y);
    }
    
    public String pieceString(int x, int y) {
        String myString;
        if(myBoard.charAt(x,y) == '\0') {
            myString = "_";
        } else {
            myString = String.valueOf(myBoard.charAt(x,y));
        }
        myString = myString + Integer.toString(x) + Integer.toString(y);
        return myString;
    }
    
    public void kingMe(int x1, int y1) {
        if(myBoard.GetPosition(x1, y1) == 'r') {
            myBoard.SetPosition(x1, y1, 'R');
        } else if (myBoard.GetPosition(x1, y1) == 'b') {
            myBoard.SetPosition(x1, y1, 'B');
        }
    }
    
    Board getBoard() {
        return myBoard;
    }
    
    /**
     * Test driver.
     *
     * @param argv
     */
    public static void main(java.lang.String[] argv) {
        Board myBoard = new Board();
        Checkers cLogic = new Checkers(myBoard);
        
        // Print initial board position
        System.out.println(cLogic.getBoard());
        cLogic.move(2, 2, 3, 3); // red moves simply
        
        System.out.println(cLogic.getBoard());
        cLogic.move(5, 5, 4, 4); // black moves simply
        
        System.out.println(cLogic.getBoard());
        //cLogic.move(4, 4, 5, 5); // illegal move -- backwards direction
        cLogic.move(3, 3, 5, 5); // red jumps black
        
        System.out.println(cLogic.getBoard());
        //cLogic.move(0, 0, 2, 2); // illegal move -- red jumps red
        //cLogic.move(1, 1, 3, 3); // illegal move -- red jumps empty space
        //System.out.println(cLogic.getBoard());
        
        // test for kinging
        // test behavior of a king
        //
        
        
    }
}