import java.util.*;
import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.concurrent.*;

/*
 * Player.java
 *
 * Created on January 13, 2007, 8:45 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author Austin
 */
public class Player {
    
    private PlayerGame myGame;
    
    private String host;
    
    private String protocol;
    
    private int port;
    
    private String yourName;
    
    private String otherName;
    
    private String gameName;
    
    BaseConnection myConnection;
    /** Creates a new instance of Player */
    public Player() {
        
        
    }
    
    public Player(String myString) {
        
        
    }
    
    public void startGame() {
            Thread t = new Thread(new PlayerGame(yourName, otherName, gameName, myConnection));
            t.start();
    }
    
    public static void main(String args[]) {
        Player Me = new Player();
        
        Me.host = args[0];
        Me.protocol = args[1];
        Me.port = Integer.parseInt(args[2]);
        Me.yourName = args[3];
        Me.otherName = args[4];
        
        if( Me.protocol.equals("TCP")) {
            Me.myConnection = new TCPConnection(Me.host, Me.port);
            
            String sendString = "i " + Me.yourName + " " + Me.otherName + " " + Me.protocol;
            Me.myConnection.sendData(sendString);
            
            
            System.out.println("waitData");
            String retVal = Me.myConnection.rcvData();
            System.out.println("endWaitData:" + retVal);
            
            if( retVal.equals("w")) {
                System.out.println("Game with " + Me.otherName + " is not available. Hold on ...");
                Me.gameName = Me.yourName + "-" + Me.otherName;
                retVal = Me.myConnection.rcvData();
                
                if(retVal.equals("c")) {
                    System.out.println("Game with " + Me.otherName + " is established ...");
                    Me.startGame();
                    
                    
                } else if (retVal.equals("a")) {
                    System.out.println("An error has occured. Now quitting.");
                }
                
                
                
                
            } else if(retVal.equals("a")) {
                System.out.println("Player " + Me.otherName + "is currently playing a game or waiting to begin a game with another player.");
                
            } else if(retVal.equals("c")) {
                System.out.println("Game with " + Me.otherName + " is established ...");
                Me.gameName = Me.otherName + "-" + Me.yourName;
                Me.startGame();
                
            }
            
            
            
            
        } else if( Me.protocol == "UDP") {
            // Me.myConnection = new UDPConnection():
        }
        
    }
    
}
