import java.util.*;
import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.concurrent.*;

public class TCPInit implements Runnable {
    ServerSocket initSocket;
    
    BufferedReader TCPReader;
    
    DataOutputStream TCPOutputStream;
    
    Server server;
    
    
    public TCPInit(Server gameServer,int initPort) {
        initSocket = null;
        TCPReader = null;
        TCPOutputStream = null;
        server = gameServer;
        
        try {
            initSocket = new ServerSocket(initPort);
        } catch (Exception e) {
            System.out.println("E" + e.getMessage());
        }
    }
    
    public void run() {
        while(true) {
            Socket aSocket = null;
            BaseConnection aConnection = null;
            String p1Name =  null;
            String p2Name = null;
            try {
                aSocket = initSocket.accept();
                aConnection = new TCPConnection(aSocket);
                String inString = aConnection.rcvData();
                
                int i;
                
                // Gets the character representing the command
                i = inString.indexOf(' ');
                String command = inString.substring(0,i);
                
                //If command = "i" for "initiate", continue
                if(command.equals("i")) {
                    // get first player's name'
                    inString = inString.substring(i + 1);
                    i = inString.indexOf(' ');
                    p1Name = inString.substring(0,i);
                    
                    // get second player's name'
                    inString = inString.substring(i + 1);
                    i = inString.indexOf(' ');
                    p2Name = inString.substring(0,i);
                }
                
                // Attemt to initiate a game
                server.initGame(p1Name, p2Name, aConnection);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
}