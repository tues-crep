#!/usr/bin/env python

from optparse import OptionParser

from aperiot.parsergen import build_parser
from splnormalizer import normalize
from evaluate import evaluate


def makeCommandlineParser():
    p = OptionParser("usage: %prog [options] file")
    p.add_option("-v", "--verbose",
                 dest="verbose", action="store_true", default=False,
                 help="Output the results of each line of code.")
    p.add_option("-t", "--tree",
                 dest="tree", action="store_true", default=False,
                 help="Output the parse tree.")
    return p

def main():
    p = makeCommandlineParser()
    (options, args) = p.parse_args()
    
    my_parser = build_parser('spl')
    
    text_to_parse = '\n'.join(open(args[0]).readlines())
    text_to_parse = normalize(text_to_parse)
    
    parse_tree = my_parser.parse(text_to_parse)
    
    if options.tree:
        print "Parse Tree:\n", 80*'-'
        def printNode(node, indent):
            if type(node[0]) == list:
                for n in node:
                    printNode(n, indent)
            else:
                print indent*'   ', node[0].__name__
                for parameter in node[1:]:
                    if type(parameter) == list:
                        printNode(parameter, indent + 1)
                    else:
                        print (indent + 1) * '   ', parameter
        for node in parse_tree:
            printNode(node, 0)
        print 80*'-'
    
    verbose_results = []
    for statement in parse_tree:
        verbose_results = verbose_results + [evaluate(statement)]
    if options.verbose:
        print "\nVerbose Results:\n", 80*'-'
        for result in verbose_results:
            print result
        print 80*'-'


if __name__ == "__main__":
    main()

# eof
