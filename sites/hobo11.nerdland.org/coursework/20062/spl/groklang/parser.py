#!/usr/bin/env python
#import commands
#import commandtree
import re

from optparse import OptionParser

"""
Load the file to parse, and translate to commands
and string arguments.  Here is an example program
that builds a simple tree of only one word in the
vocabulary, listed with two definitions.

simpletree.decl::
    add german:machen english:to do
    add german:machen english:to make
    lis german:machen
    del german:machen english: to make
    sel german: machen
    del german:machen

Note that the spaces are only significant after the
initial command keyword and also inside of a phrase,
so on line 5 " machen" refers to the same node as
line 6 "machen".  Language names are not allowed to
have spaces.  With these rules, the most bizarre
command that I can think of is still easy to parse:

add german:  mach einen   weg  english:make  a way

simplilfies down to this...

add german:mach einen   weg english:make  a way

which produces this node for the vocabulary...
<german name="mach einen   weg">
    <english name="make  a way"/>
</german>

As said before be careful using spaces internally
within a phrase, as they are significant.
"""

def makeOptionParser():
    o = OptionParser("usage: %prog file")
    return o

def main():
    o = makeOptionParser()
    (options, args) = o.parse_args()
    f = open(args[0])
    lines = f.readlines()

    p = Parser(lines)

    x = 0
    for line in lines:
	x+=1
	print "%d:\t" % x + line.strip()



class Parser:
    myCommands=[]

    def __init__(self):
	pass

    def readFile(self, fileName):
	pass	
	
    def runCommand(self):
	pass


if __name__ == "__main__":
    main()

