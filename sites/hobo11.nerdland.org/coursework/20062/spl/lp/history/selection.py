#!/usr/bin/env python

from evaluate import evaluate

def select(parameters):
    condition = evaluate(parameters[0])
    trueBranch = parameters[1]
    falseBranch = parameters[2]
    
    results = []
    
    if condition:
        for statement in trueBranch:
            results = results + [evaluate(statement)]
    else:
        if falseBranch != None:
            for statement in falseBranch:
                results = results + [evaluate(statement)]
    
    return results

# eof