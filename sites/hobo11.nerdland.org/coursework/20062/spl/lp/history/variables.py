#!/usr/bin/env python

from evaluate import evaluate
import symboltable

def assignVariable(parameters):
    identifier = parameters[0]
    value = evaluate(parameters[1])
    
    symboltable.global_symbols.assignSymbol(identifier, value)

def getVariable(parameters):
    identifier = parameters[0]
    
    return symboltable.global_symbols.getSymbol(identifier)

# eof