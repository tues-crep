#!/usr/bin/env python

class SymbolTable:
    def __init__(self):
        self.table = {}
    
    def assignSymbol(self, identifier, value):
        self.table[identifier] = value
    
    def getSymbol(self, identifier):
        return self.table[identifier]
    
    def dumpTable(self):
        print self.table

global_symbols = SymbolTable()

# eof