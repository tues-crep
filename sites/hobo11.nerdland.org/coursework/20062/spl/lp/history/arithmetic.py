#!/usr/bin/env python

from evaluate import evaluate

def add(parameters):
    x = evaluate(parameters[0])
    y = evaluate(parameters[1])
    return x + y

def subtract(parameters):
    x = evaluate(parameters[0])
    y = evaluate(parameters[1])
    return x - y

def multiply(parameters):
    x = evaluate(parameters[0])
    y = evaluate(parameters[1])
    return x * y

def divide(parameters):
    x = evaluate(parameters[0])
    y = evaluate(parameters[1])
    return x / y

def number(parameters):
    n = parameters[0]
    return n

# eof