#!/usr/bin/env python

from evaluate import evaluate

def isequalto(parameters):
    x = evaluate(parameters[0])
    y = evaluate(parameters[1])
    
    return x == y

def islessthan(parameters):
    x = evaluate(parameters[0])
    y = evaluate(parameters[1])
    
    return x < y

def isgreaterthan(parameters):
    x = evaluate(parameters[0])
    y = evaluate(parameters[1])
    
    return x > y

def islessthanorequalto(parameters):
    x = evaluate(parameters[0])
    y = evaluate(parameters[1])
    
    return x <= y

def isgreaterthanorequalto(parameters):
    x = evaluate(parameters[0])
    y = evaluate(parameters[1])
    
    return x >= y

def isnotequalto(parameters):
    x = evaluate(parameters[0])
    y = evaluate(parameters[1])
    
    return not x == y

def isnotlessthan(parameters):
    x = evaluate(parameters[0])
    y = evaluate(parameters[1])
    
    return not x < y

def isnotgreaterthan(parameters):
    x = evaluate(parameters[0])
    y = evaluate(parameters[1])
    
    return not x > y

def isnotlessthanorequalto(parameters):
    x = evaluate(parameters[0])
    y = evaluate(parameters[1])
    
    return not x <= y

def isnotgreaterthanorequalto(parameters):
    x = evaluate(parameters[0])
    y = evaluate(parameters[1])
    
    return not x >= y

# eof