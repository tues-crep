#!/usr/bin/env python

def normalize(code):
    """Takes a string of code and replaces stuff."""
    symbol_table = { \
        "divided by":                       "divided_by", \
        "check that":                       "check_that", \
        "is equal to":                      "is_equal_to", \
        "is less than":                     "is_less_than", \
        "is greater than":                  "is_greater_than", \
        "than or equal to":                 "than_or_equal_to", \
        "is not equal to":                  "is_not_equal_to", \
        "is not less than":                 "is_not_less_than", \
        "is not greater than":              "is_not_greater_than", \
        "end if":                           "end_if" }
    
    for original in symbol_table:
        code = code.replace(original, symbol_table[original])
    
    return code

# eof