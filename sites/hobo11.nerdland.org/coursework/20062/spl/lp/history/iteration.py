#!/usr/bin/env python

from evaluate import evaluate

def whileLoop(parameters):
    condition = parameters[0]
    statements = parameters[1]
    
    results = []
    
    while(evaluate(condition)):
        run_results = []
        for statement in statements:
            run_results = run_results + [evaluate(statement)]
        results = results + [run_results]
    
    return results

# eof