#! /usr/bin/env python

"""
CP3 Genotype / Phenotype

Kingdon Barrett
Andrew Bair
01/11/2007
Genetic Algorithms
Professor Joe Geigel
"""

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from pygene.gene import FloatGene
from pygene.organism import Organism
from pygene.population import Population
from math import fabs
import sys

class CameraGene(FloatGene):
    """
    Gene which represents the numbers used in our organism
    """
    # genes get randomly generated within this range
    randMin = -50.0
    randMax = 50.0

class Camera(Organism):
    """
    Implements the camera organism
    
    Gene 1: camera eye x
    Gene 2: camera eye y
    Gene 3: camera center x
    Gene 4: camera center y
    Gene 5: zoom factor
    """
    genome = dict([(str(i), CameraGene) for i in range(1, 6)])

    orgsBorn = 0

    def __init__(self, **kw):
	Organism.__init__(self)
	Camera.orgsBorn += 1
	self.id = Camera.orgsBorn

	# NB: pygene is misbehaving!  It always produces 4x childCount,
	# so in fact 40 children are produced for a childCount of 10!
	# Hopefully we can fix this before CP3, and submit a patch to
	# the pygene people.
	#print "new org #" + str(Maximizer.orgsBorn)

    def fitness(self):
	"""
	Implements the 'fitness function' for this species.
	In pygene, Organisms try to evolve to MINIMIZE this
	function's value.
	"""
	# The hardest part of this project will be creating this fitness function.
	# Specifically, we're going to need a way to determine if the objects are
	# within the view of the camera.  Perhaps we can cast four rays from the
	# camera to the z = 0 plane to form four corners a rectangular region.  We
	# could then take the x and y values of these corners and do a simple
	# comparison to the x y position of the objects to see if the objects are
	# within the rectangular region (and therefore, in the camera's view).
	# Getting all of the objects on the screen is our first priority.  Only
	# after all of the objects are on the screen should we worry about evolving
	# cameras that can zoom in closer.  For this checkpoint, we're just going
	# to worry about getting the camera's eye and center values as close to
	# (0, 0, z) as we can (the object is stationary at (0, 0, 0) for this
	# checkpoint)
	return (fabs(self['1']) + fabs(self['2']) + fabs(self['3']) +
		fabs(self['4']))

    def __repr__(self):
	"""
	Printable representation of a single organism
	"""
	repr = "<Camera id = %s\n"
	repr += "    camera_x = %s, camera_y = %s\n"
	repr += "    center_x = %s, center_y = %s\n"
	repr += "    zoom = %s, fitness = %s>"
	repr %= (self.id, self['1'], self['2'], self['3'], self['4'],
		self['5'] + 50, self.fitness())
	return repr
	
class CameraPop(Population):
    initPopulation = 10
    childCull = 10
    childCount = 10
    incest = 10

    species = Camera

    def __init__(self):
	Population.__init__(self)
	popId = 0
	self.genId = 1

    def gen(self):
	Population.gen(self)
	self.genId += 1

    def __repr__(self):
	repr = ""
	repr += "[Generation #" + str(self.genId) + "\n"
	for i in range(self.__len__()):
	    repr += str(self[i]) + "\n"
	repr += "]"
	return repr

# create a fresh population
pop = CameraPop()

# OpenGL stuff
screenWidth = 500
screenHeight = 500
eyeX = 0
eyeY = 0
centerX = 0
centerY = 0
zoom = 1

# display our scene
def display():
    global eyeX
    global eyeY
    global centerX
    global centerY
    global zoom

    glClear(GL_COLOR_BUFFER_BIT)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()

    # these two GLU commands are our phenotype
    gluPerspective(100.0 / zoom, 1.0, 1.0, 1000.0)
    gluLookAt(eyeX, eyeY, 50, centerX, centerY, 0, 0, 1, 0)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    # the white quad shows the boundaries for the objects and the camera's
    # center position:
    # x = [-50, 50], y = [-50, 50], z = 0
    # the camera's eye position will likely be bounded to:
    # x = [-50, 50], y = [-50, 50], z = 50
    glColor3ub(255, 255, 255)
    glBegin(GL_QUADS)
    glVertex2i(50, 50)
    glVertex2i(-50, 50)
    glVertex2i(-50, -50)
    glVertex2i(50, -50)
    glEnd()

    # the red cube is our target object for this checkpoint
    glColor3ub(255, 0, 0)
    glutSolidCube(1.0)

    glutSwapBuffers()

# get keyboard input
def keyboard(*args):
    if args[0] == '\033':
        glutDestroyWindow(glutGetWindow())

# do stuff when idle
def idle():
    glutPostRedisplay()

# now a func to run the population
def main():
    global screenWidth
    global screenHeight
    global eyeX
    global eyeY
    global centerX
    global centerY
    global zoom

    # initialize OpenGL and GLUT
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE)
    glutInitWindowSize(screenWidth, screenHeight)
    glutInitWindowPosition(0, 0)
    glutCreateWindow("Attention Lens")
    glutDisplayFunc(display)
    glutIdleFunc(idle)
    glutKeyboardFunc(keyboard)
    glEnable(GL_BLEND)
    glEnable(GL_CULL_FACE)
    glEnable(GL_DEPTH_TEST)
    glFrontFace(GL_CCW)
    glCullFace(GL_BACK)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    glClearColor(0.5, 0.5, 0.5, 1.0)
    glDepthFunc(GL_LEQUAL)

    bestFitness = dict([(i, 0) for i in range(1, 101)])

    while pop.genId<=100:
	# print the current generation's best individual
	print "Generation %s: %s" % (pop.genId, pop.best())
	bestFitness[pop.genId] = pop.best().fitness()
	# execute a generation
	pop.gen()
	# get the fittest member
	best = pop.best()

    # print out the best fitnesses (so I can copy into Excel)
    for i in range(1, 101):
	    print bestFitness[i]

    #print pop.best()
    eyeX = pop.best()['1']
    eyeY = pop.best()['2']
    centerX = pop.best()['3']
    centerY = pop.best()['4']
#    zoom = pop.best()['5'] + 50

    glutMainLoop()

if __name__ == '__main__':
    main()

