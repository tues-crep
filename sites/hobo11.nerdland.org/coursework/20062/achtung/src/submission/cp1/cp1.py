#! /usr/bin/env python

"""
CP1 Genetic Algorithm Framework

Kingdon Barrett
Andrew Bair
01/11/2007
Genetic Algorithms
Professor Joe Geigel

This ga seeks to maximize the 2-dimensional
function:

f(x,y) = x^2 + y^2

The floating point values of x and y are
restricted to the range [-5, 5].
"""

from pygene.gene import FloatGene, FloatGeneMax
from pygene.organism import Organism, MendelOrganism
from pygene.population import Population

class MaxerGene(FloatGene):
    """
    Gene which represents the numbers used in our organism
    """
    # genes get randomly generated within this range
    randMin = -5.0
    randMax = 5.0

    # degree of mutation (will mutate up to the constant value 1.0)
    mutAmt = 1.0 / (randMax-randMin)

    # probability of mutation
    # NB: this is not exactly choosing one child at random per
    # generation to mutate.  However, on average it will have
    # that effect.
    mutProb = 0.1

class Maximizer(Organism):
    """
    Implements the organism which tries
    to maximize a function
    """
    genome = {'1':MaxerGene, '2':MaxerGene}

    orgsBorn = 0

    def __init__(self, **kw):
	Organism.__init__(self)
	Maximizer.orgsBorn += 1
	self.id = Maximizer.orgsBorn

	# NB: pygene is misbehaving!  It always produces 4x childCount,
	# so in fact 40 children are produced for a childCount of 10!
	# Hopefully we can fix this before CP3, and submit a patch to
	# the pygene people.
	#print "new org #" + str(Maximizer.orgsBorn)

    def fitness(self):
	"""
	Implements the 'fitness function' for this species.
	In pygene, Organisms try to evolve to MINIMIZE this
	function's value.
	"""
	return (5**2+5**2)-self.theFunc()

    def theFunc(self):
	"""
	Implements the function that we are trying to MAXIMIZE.
	"""
	return (self['1'] ** 2)+(self['2'] ** 2)
	
    def __repr__(self):
	"""
	Printable representation of a single organism.
	"""

	# NB: We are using theFunc rather than fitness here, to produce
	# the expected output where f(x[1],x[2])=fitness
	return "<Maximizer id=%s x[1]=%s x[2]=%s fitness=%f>" % (
	    self.id, self['1'], self['2'], self.theFunc())

class MaxerPop(Population):
    initPopulation = 10
    childCull = 10
    childCount = 10
    incest = 10
    
    species = Maximizer

    def __init__(self):
	Population.__init__(self)
	popId = 0
	self.genId = 1

    def gen(self):
	Population.gen(self)
	self.genId += 1

    def __repr__(self):
	repr = ""
	repr += "[Generation #" + str(self.genId) + "\n"
	for i in range(self.__len__()):
	    repr += str(self[i]) + "\n"
	repr += "]"
	return repr

# create a fresh population

pop = MaxerPop()

# now a func to run the population

def main():
    while pop.genId<10:
	# print the current generation
	print pop
	# execute a generation
	pop.gen()
	# get the fittest member
	best = pop.best()

    print pop

if __name__ == '__main__':
    main()

