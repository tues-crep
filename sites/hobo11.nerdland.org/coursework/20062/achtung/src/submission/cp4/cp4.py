#! /usr/bin/env python

"""
CP4 Fitness and Selection

Kingdon Barrett
Andrew Bair
01/30/2007
Genetic Algorithms
Professor Joe Geigel
"""

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from pygene.gene import FloatGene
from pygene.organism import Organism
from pygene.population import Population
from math import fabs
import sys

class CameraGene(FloatGene):
    """
    Gene which represents the numbers used in our organism
    """
    # genes get randomly generated within this range
    randMin = -50.0
    randMax = 50.0

class Camera(Organism):
    """
    Implements the camera organism
    
    Gene 0: camera eye x
    Gene 1: camera eye y
    Gene 2: camera center x
    Gene 3: camera center y
    Gene 4: zoom factor
    """
    genome = dict([(str(i), CameraGene) for i in range(0, 5)])
    fitnessScore = 100
    orgsBorn = -1

    def __init__(self, **kw):
        Organism.__init__(self)
        Camera.orgsBorn += 1
        self.id = Camera.orgsBorn

        # NB: pygene is misbehaving!  It always produces 4x childCount,
        # so in fact 40 children are produced for a childCount of 10!
        # Hopefully we can fix this before CP3, and submit a patch to
        # the pygene people.
        #print "new org #" + str(Maximizer.orgsBorn)

    def fitness(self):
        """
        Implements the 'fitness function' for this species.
        In pygene, Organisms try to evolve to MINIMIZE this
        function's value.
        """
        return self.fitnessScore
        
    def setFitnessScore(self, fitnessScore):
        self.fitnessScore = fitnessScore

    def __repr__(self):
        """
        Printable representation of a single organism
        """
        repr = "<Camera id = %s\n"
        repr += "    camera_x = %s, camera_y = %s\n"
        repr += "    center_x = %s, center_y = %s\n"
        repr += "    zoom = %s, fitness = %s>"
        repr %= (self.id, self['0'], self['1'], self['2'], self['3'],
            self['4'] + 50, self.fitness())
        return repr

class CameraPop(Population):
    initPopulation = 10
    childCull = 10
    childCount = 10
    incest = 10

    species = Camera

    def __init__(self):
        Population.__init__(self)
        popId = 0
        self.genId = 0
    
    def gen(self):
        Population.gen(self)
        self.genId += 1
    
    def __repr__(self):
        repr = ""
        repr += "[Generation #" + str(self.genId) + "\n"
        for i in range(self.__len__()):
            repr += str(self[i]) + "\n"
        repr += "]"
        return repr

# does nothing - here just to make glut happy
def display():
    glutSwapBuffers()

# create a fresh population
pop = CameraPop()

# keeps track of the current organism
currentOrganism = 0

# number of generations to evolve
nGenerations = 10

# screen dimensions
screenWidth = 240
screenHeight = 240

# do stuff when idle
def idle():
    global currentOrganism
    global pop
    global screenWidth
    global screenHeight

    glClear(GL_COLOR_BUFFER_BIT)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()

    if currentOrganism == -1:
        eyeX = pop.best()['0']
        eyeY = pop.best()['1']
        centerX = pop.best()['2']
        centerY = pop.best()['3']
        zoom = pop.best()['4']
    else:
        eyeX = pop[currentOrganism]['0']
        eyeY = pop[currentOrganism]['1']
        centerX = pop[currentOrganism]['2']
        centerY = pop[currentOrganism]['3']
        zoom = pop[currentOrganism]['4'] + 50

    gluPerspective(100.0 / zoom, 1.0, 1.0, 1000.0)
    gluLookAt(eyeX, eyeY, 50.0, centerX, centerY, 0.0, 0.0, 1.0, 0.0)
    
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    
    glColor3ub(255, 255, 255)
    glBegin(GL_QUADS)
    glVertex2i(50, 50)
    glVertex2i(-50, 50)
    glVertex2i(-50, -50)
    glVertex2i(50, -50)
    glEnd()
    
    nTargets = 5
    colors = dict([(i, (0, 0, 0)) for i in range(0, nTargets)])
    colors[0] = (255, 0, 0)
    colors[1] = (0, 255, 0)
    colors[2] = (0, 0, 255)
    colors[3] = (255, 255, 0)
    colors[4] = (0, 255, 255)

    positions = dict([(i, (0.0, 0.0, 0.0)) for i in range(0, nTargets)])
    positions[0] = (0.0, 0.0, 0.0)
    positions[1] = (30.0, 30.0, 0.0)
    positions[2] = (-30.0, 30.0, 0.0)
    positions[3] = (30.0, -30.0, 0.0)
    positions[4] = (-30.0, -30.0, 0.0)


    glBegin(GL_QUADS)
    for i in range (0, nTargets):
        glColor3ubv(colors[i])
        glVertex2f(positions[i][0] + 4, positions[i][1] + 4)
        glVertex2f(positions[i][0] - 4, positions[i][1] + 4)
        glVertex2f(positions[i][0] - 4, positions[i][1] - 4)
        glVertex2f(positions[i][0] + 4, positions[i][1] - 4)
    glEnd()

    targetViewable = dict([(i, 0) for i in range(0, nTargets)])
    targetWeight = 10
    fitnessScore = nTargets * targetWeight * 2
    pixels = glReadPixelsub(0, 0, screenWidth, screenHeight, GL_RGB)
    for i in range(0, screenWidth):
        for j in range(0, screenHeight):
            for k in range(0, nTargets):
                if ord(pixels[i][j][0]) == colors[k][0]:
                    if ord(pixels[i][j][1]) == colors[k][1]:
                        if ord(pixels[i][j][2]) == colors[k][2]:
                            targetViewable[k] += 1

    for i in range(0, nTargets):
        if targetViewable[i] != 0:
            fitnessScore -= targetWeight

    if fitnessScore > (nTargets * targetWeight):
        fitnessScore += zoom
    else:
        fitnessScore -= zoom

    pop[currentOrganism].setFitnessScore(fitnessScore)

    if currentOrganism != -1:
        currentOrganism += 1
        if currentOrganism >= pop.__len__():
            if pop.genId <= nGenerations:
                currentOrganism = 0
                print pop
                print pop.best()
                pop.gen()
            else:
                currentOrganism = -1

    glutSwapBuffers()

# now a func to run the population
def main():
    global screenWidth
    global screenHeight

    # initialize OpenGL and GLUT
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE)
    glutInitWindowSize(screenWidth, screenHeight)
    glutInitWindowPosition(0, 0)
    glutCreateWindow("Attention Lens")
    glutDisplayFunc(display)
    glutIdleFunc(idle)
    glEnable(GL_BLEND)
    glEnable(GL_CULL_FACE)
    glEnable(GL_DEPTH_TEST)
    glFrontFace(GL_CCW)
    glCullFace(GL_BACK)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    glClearColor(0.5, 0.5, 0.5, 1.0)
    glDepthFunc(GL_LEQUAL)

    glutMainLoop()

if __name__ == '__main__':
    main()

