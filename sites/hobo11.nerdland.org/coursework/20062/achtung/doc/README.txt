Andrew Bair
Kingdon Barrett
Genetic Algorithms CP1
Professor Joe Geigel

The software was developed using Python (cross-platform), on a Mac Mini 1.42GHz
PowerPC using the pyGene and pyOpenGL libraries.  I imagine you'll be running
this on your Mac.  In that case, you'll need Python 2.4, pyGene, and pyOpenGL.

Python 2.4 is available here:
http://pythonmac.org/packages/py24-fat/dmg/python-2.4.4-macosx2006-10-18.dmg

pyGene is available here:
http://www.freenet.org.nz/python/pygene/pygene-0.2.1.tar.gz

pyOpenGL is available for Mac here:
http://pythonmac.org/packages/py24-fat/mpkg/PyOpenGL-2.0.2.01-py2.4-macosx10.4.zip

On a Linux PC, some externally supported libraries are required.

Ubuntu Instructions:
apt-get install python-opengl

To run our code:
$ python cp3_class1.py
or
$ python cp3_class2.py

cp3_class1.py is for Class 1 genotype, cp3_class2.py is for Class 2 genotype.
The tiff files included show the best individual camera after 100 generations
for each class.
