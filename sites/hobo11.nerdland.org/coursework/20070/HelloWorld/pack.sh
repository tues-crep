#!/usr/bin/env bash
# Example build script to create an App folder using a single resource nib
# and a CLR assembly.  -m:console does not do what I expect it to do here.
macpack -m:2 -n:HelloWorld -o:. -a:helloworld.exe -r:helloworld.nib
