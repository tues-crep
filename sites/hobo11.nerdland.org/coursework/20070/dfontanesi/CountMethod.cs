using System;
using System.Collections;

namespace BlackJack
{
	/// <summary>
	/// Summary description for CountMethod.
	/// </summary>
	public abstract class CountMethod
	{
		protected CountMethod( int numberOfDecks )
		{
			Decks = numberOfDecks;
		}

		protected string name = "";
		protected int level = 0;

		protected int Decks;
		protected double runningTotal;
		protected int cardCount;
		protected int acesCount;
		protected int twosCount;
		protected int threesCount;
		protected int foursCount;
		protected int fivesCount;
		protected int sixesCount;
		protected int sevensCount;
		protected int eightsCount;
		protected int ninesCount;
		protected int tensCount;

		protected virtual double[] GetCounts() { return null; }
		protected int SideCount( Card.CardType cardType )
		{
			int count = 0;

			switch( cardType )
			{
				case Card.CardType.Ace:
					count = acesCount;
					break;
				case Card.CardType.Two:
					count = twosCount;
					break;
				case Card.CardType.Three:
					count = threesCount;
					break;
				case Card.CardType.Four:
					count = foursCount;
					break;
				case Card.CardType.Five:
					count = fivesCount;
					break;
				case Card.CardType.Six:
					count = sixesCount;
					break;
				case Card.CardType.Seven:
					count = sevensCount;
					break;
				case Card.CardType.Eight:
					count = eightsCount;
					break;
				case Card.CardType.Nine:
					count = ninesCount;
					break;
				case Card.CardType.Ten:
					count = tensCount;
					break;
			}

			return count;
		}
		public virtual double GetWager( double normalBet )
		{
			double wager = 0;
			double trueCount = Count;

			if( trueCount > 0 )
				wager = normalBet * trueCount;
			else if( trueCount == 0 )
				wager = normalBet;
			else if( trueCount < 0 )
				wager = normalBet * trueCount;

			// $10 table minimum :)  Also, round to nearest integer
			wager = (int)Math.Max( 10, wager );

			return wager;
		}

		public virtual string MethodName{ get{ return "";}}
		public virtual int MethodLevel{ get{ return 0; }}
		public virtual void CountCard( Card card )
		{
			runningTotal += GetCounts()[ (int)card.TrueValue ];
			cardCount++;

			// Now keep side counts for all cards
			switch( card.FaceValue )
			{
				case Card.CardType.Ace:
					acesCount++;
					break;
				case Card.CardType.Two:
					twosCount++;
					break;
				case Card.CardType.Three:
					threesCount++;
					break;
				case Card.CardType.Four:
					foursCount++;
					break;
				case Card.CardType.Five:
					fivesCount++;
					break;
				case Card.CardType.Six:
					sixesCount++;
					break;
				case Card.CardType.Seven:
					sevensCount++;
					break;
				case Card.CardType.Eight:
					eightsCount++;
					break;
				case Card.CardType.Nine:
					ninesCount++;
					break;
				case Card.CardType.Ten:
					tensCount++;
					break;
				case Card.CardType.Jack:
					tensCount++;
					break;
				case Card.CardType.Queen:
					tensCount++;
					break;
				case Card.CardType.King:
					tensCount++;
					break;
			}
		}

		public virtual void Reset( int decks )
		{
			cardCount = 0;
			runningTotal = 0;
			acesCount = 0;
			twosCount = 0;
			threesCount = 0;
			foursCount = 0;
			fivesCount = 0;
			sixesCount = 0;
			sevensCount = 0;
			eightsCount = 0;
			ninesCount = 0;
			tensCount = 0;
			Decks = decks;
		}

		public virtual double Count
		{
			get{ return runningTotal / (( Decks*52 - cardCount ) / 52 ); }
		}

		public static ArrayList GetMethods()
		{
			ArrayList methods = new ArrayList();

			methods.Add( "Hi-Lo" );
			methods.Add( "High-Low" );
			methods.Add( "Hi Opt I" );
			methods.Add( "Hi Opt II" );
			methods.Add( "Silver Fox" );
			methods.Add( "Brh I" );
			methods.Add( "Brh II" );
			methods.Add( "Canfield Expert" );
			methods.Add( "Canfield Master" );
			methods.Add( "KO" );
			methods.Add( "Omega II" );
			methods.Add( "Red Seven" );
			methods.Add( "Revere Adv. Plus Minus" );
			methods.Add( "Revere Point Count" );
			methods.Add( "Unb. Zen 11" );
			methods.Add( "Uston Adv. Plus Minus" );
			methods.Add( "Uston APC" );
			methods.Add( "Uston SS" );
			methods.Add( "Wong Halves" );
			methods.Add( "Zen Count" );
			methods.Add( "Hi-Lo Ace Side Count" );
			methods.Add( "HiOptI Ace-Seven Side Count" );

			return methods;
		}
		public static CountMethod NewMethod( string methodName, int n )
		{
			CountMethod returnValue = null;

			switch( methodName )
			{
				case( "Hi-Lo" ):
					returnValue = new HiLo( n );
					break;
				case( "High-Low" ):
					returnValue = new HighLow( n );
					break;
				case( "Hi Opt I" ):
					returnValue = new HiOptI( n );
					break;
				case( "Hi Opt II" ):
					returnValue = new HiOptII( n );
					break;
				case( "Silver Fox" ):
					returnValue = new SilverFox( n );
					break;
				case( "Brh I" ):
					returnValue = new BrhI( n );
					break;
				case( "Brh II" ):
					returnValue = new BrhII( n );
					break;
				case( "Canfield Expert" ):
					returnValue = new CanfieldExpert( n );
					break;
				case( "Canfield Master" ):
					returnValue = new CanfieldMaster( n );
					break;
				case( "KO" ):
					returnValue = new KO( n );
					break;
				case( "Omega II" ):
					returnValue = new OmegaII( n );
					break;
				case( "Red Seven" ):
					returnValue = new RedSeven( n );
					break;
				case( "Revere Adv. Plus Minus" ):
					returnValue = new RevereAdvPlusMinus( n );
					break;
				case( "Revere Point Count" ):
					returnValue = new ReverePointCount( n );
					break;
				case( "Unb. Zen 11" ):
					returnValue = new UnbZen11( n );
					break;
				case( "Uston Adv. Plus Minus" ):
					returnValue = new UstonAdvPlusMinus( n );
					break;
				case( "Uston APC" ):
					returnValue = new UstonApc( n );
					break;
				case( "Uston SS" ):
					returnValue = new UstonSS( n );
					break;
				case( "Wong Halves" ):
					returnValue = new WongHalves( n );
					break;
				case( "Zen Count" ):
					returnValue = new ZenCount( n );
					break;
				case( "Hi-Lo Ace Side Count" ):
					returnValue = new HiLoA( n );
					break;
				case( "HiOptI Ace-Seven Side Count" ):
					returnValue = new HiOptIA7( n );
					break;
				default:
					returnValue = null;
					break;
			}

			return returnValue;
		}
	}

	public class HiLo : CountMethod
	{
		public HiLo( int n ) : base( n )
		{
		}

		protected override double[] GetCounts() { return counts; }
		// Card counts                             A  2  3  4  5  6  7  8  9  T
		protected double[] counts = new double[] { 0, 0, 1, 1, 1, 1, 0, 0, 0,-1 };
		new private string name = "Hi-Lo";
		new private int level = 1;
		public override string MethodName{ get{ return name; }}
		public override int MethodLevel{ get{ return level; }}
	}

	public class HighLow : CountMethod
	{
		public HighLow( int n ) : base( n )
		{
		}

		protected override double[] GetCounts() { return counts; }
		// Card counts                             A  2  3  4  5  6  7  8  9  T
		protected double[] counts = new double[] {-1, 1, 1, 1, 1, 1, 0, 0, 0,-1 };
		new private string name = "High-Low";
		new private int level = 1;
		public override string MethodName{ get{ return name; }}
		public override int MethodLevel{ get{ return level; }}
	}

	public class HiOptI : CountMethod
	{
		public HiOptI( int n ) : base( n )
		{
		}

		protected override double[] GetCounts() { return counts; }
		// Card counts                             A  2  3  4  5  6  7  8  9  T
		protected double[] counts = new double[] { 0, 0, 1, 1, 1, 1, 0, 0, 0,-1 };
		new private string name = "HiOptI";
		new private int level = 1;
		public override string MethodName{ get{ return name; }}
		public override int MethodLevel{ get{ return level; }}
	}

	public class HiOptII : CountMethod
	{
		public HiOptII( int n ) : base( n )
		{
		}

		protected override double[] GetCounts() { return counts; }
		// Card counts                             A  2  3  4  5  6  7  8  9  T
		protected double[] counts = new double[] { 0, 1, 1, 2, 2, 1, 1, 0, 0,-2 };
		new private string name = "HiOptII";
		new private int level = 2;
		public override string MethodName{ get{ return name; }}
		public override int MethodLevel{ get{ return level; }}
	}

	public class SilverFox : CountMethod
	{
		public SilverFox( int n ) : base( n )
		{
		}

		protected override double[] GetCounts() { return counts; }
		// Card counts                             A  2  3  4  5  6  7  8  9  T
		protected double[] counts = new double[] {-1, 1, 1, 1, 1, 1, 1, 0,-1,-1 };
		new private string name = "Silver Fox";
		new private int level = 1;
		public override string MethodName{ get{ return name; }}
		public override int MethodLevel{ get{ return level; }}
	}
	
	public class BrhI : CountMethod
	{
		public BrhI( int n ) : base( n )
		{
		}

		protected override double[] GetCounts() { return counts; }
		// Card counts                             A  2  3  4  5  6  7  8  9  T
		protected double[] counts = new double[] {-2, 1, 2, 2, 3, 2, 1, 0, 0,-2 };
		new private string name = "BrhI";
		new private int level = 3;
		public override string MethodName{ get{ return name; }}
		public override int MethodLevel{ get{ return level; }}
	}
	
	public class BrhII : CountMethod
	{
		public BrhII( int n ) : base( n )
		{
		}

		protected override double[] GetCounts() { return counts; }
		// Card counts                             A  2  3  4  5  6  7  8  9  T
		protected double[] counts = new double[] {-2, 1, 1, 2, 2, 2, 1, 0, 0,-2 };
		new private string name = "BrhII";
		new private int level = 2;
		public override string MethodName{ get{ return name; }}
		public override int MethodLevel{ get{ return level; }}
	}

	public class CanfieldExpert : CountMethod
	{
		public CanfieldExpert( int n ) : base( n )
		{
		}

		protected override double[] GetCounts() { return counts; }
		// Card counts                             A  2  3  4  5  6  7  8  9  T
		protected double[] counts = new double[] { 0, 0, 1, 1, 1, 1, 1, 0,-1,-1 };
		new private string name = "Canfield Expert";
		new private int level = 1;
		public override string MethodName{ get{ return name; }}
		public override int MethodLevel{ get{ return level; }}
	}
	
	public class CanfieldMaster : CountMethod
	{
		public CanfieldMaster( int n ) : base( n )
		{
		}

		protected override double[] GetCounts() { return counts; }
		// Card counts                             A  2  3  4  5  6  7  8  9  T
		protected double[] counts = new double[] { 0, 1, 1, 2, 2, 2, 1, 0,-1,-2 };
		new private string name = "Canfield Master";
		new private int level = 2;
		public override string MethodName{ get{ return name; }}
		public override int MethodLevel{ get{ return level; }}
	}
	
	public class KO : CountMethod
	{
		public KO( int n ) : base( n )
		{
		}

		protected override double[] GetCounts() { return counts; }
		// Card counts                             A  2  3  4  5  6  7  8  9  T
		protected double[] counts = new double[] {-1, 1, 1, 1, 1, 1, 1, 0, 0,-1 };
		new private string name = "KO";
		new private int level = 1;
		public override string MethodName{ get{ return name; }}
		public override int MethodLevel{ get{ return level; }}
	}
	
	public class OmegaII : CountMethod
	{
		public OmegaII( int n ) : base( n )
		{
		}

		protected override double[] GetCounts() { return counts; }
		// Card counts                             A  2  3  4  5  6  7  8  9  T
		protected double[] counts = new double[] { 0, 0, 1, 1, 1, 1, 1, 0,-1,-1 };
		new private string name = "Omega II";
		new private int level = 1;
		public override string MethodName{ get{ return name; }}
		public override int MethodLevel{ get{ return level; }}
	}
	
	public class RedSeven : CountMethod
	{
		public RedSeven( int n ) : base( n )
		{
		}

		protected override double[] GetCounts() { return counts; }
		// Card counts                             A  2  3  4  5  6  7  8  9  T
		protected double[] counts = new double[] {-1, 1, 1, 1, 1, 1,.5, 0, 0,-1 };
		new private string name = "Red Seven";
		new private int level = 1;
		public override string MethodName{ get{ return name; }}
		public override int MethodLevel{ get{ return level; }}
	}	

	public class RevereAdvPlusMinus : CountMethod
	{
		public RevereAdvPlusMinus( int n ) : base( n )
		{
		}

		protected override double[] GetCounts() { return counts; }
		// Card counts                             A  2  3  4  5  6  7  8  9  T
		protected double[] counts = new double[] { 0, 1, 1, 1, 1, 1, 0, 0,-1,-1 };
		new private string name = "Revere Adv. Plus Minus";
		new private int level = 1;
		public override string MethodName{ get{ return name; }}
		public override int MethodLevel{ get{ return level; }}
	}

	public class ReverePointCount : CountMethod
	{
		public ReverePointCount( int n ) : base( n )
		{
		}

		protected override double[] GetCounts() { return counts; }
		// Card counts                             A  2  3  4  5  6  7  8  9  T
		protected double[] counts = new double[] {-2, 1, 2, 2, 2, 2, 1, 0, 0,-2 };
		new private string name = "Revere Point Count";
		new private int level = 2;
		public override string MethodName{ get{ return name; }}
		public override int MethodLevel{ get{ return level; }}
	}

	public class UnbZen11 : CountMethod
	{
		public UnbZen11( int n ) : base( n )
		{
		}

		protected override double[] GetCounts() { return counts; }
		// Card counts                             A  2  3  4  5  6  7  8  9  T
		protected double[] counts = new double[] {-1, 1, 2, 2, 2, 2, 1, 0, 0,-2 };
		new private string name = "Unb. Zen 11";
		new private int level = 2;
		public override string MethodName{ get{ return name; }}
		public override int MethodLevel{ get{ return level; }}
	}

	public class UstonAdvPlusMinus : CountMethod
	{
		public UstonAdvPlusMinus( int n ) : base( n )
		{
		}

		protected override double[] GetCounts() { return counts; }
		// Card counts                             A  2  3  4  5  6  7  8  9  T
		protected double[] counts = new double[] {-1, 0, 1, 1, 1, 1, 1, 0, 0,-1 };
		new private string name = "Uston Adv. Plus Minus";
		new private int level = 1;
		public override string MethodName{ get{ return name; }}
		public override int MethodLevel{ get{ return level; }}
	}

	public class UstonApc : CountMethod
	{
		public UstonApc( int n ) : base( n )
		{
		}

		protected override double[] GetCounts() { return counts; }
		// Card counts                             A  2  3  4  5  6  7  8  9  T
		protected double[] counts = new double[] { 0, 1, 2, 2, 3, 2, 2, 1,-1,-3 };
		new private string name = "Uston APC";
		new private int level = 3;
		public override string MethodName{ get{ return name; }}
		public override int MethodLevel{ get{ return level; }}
	}

	public class UstonSS : CountMethod
	{
		public UstonSS( int n ) : base( n )
		{
		}

		protected override double[] GetCounts() { return counts; }
		// Card counts                             A  2  3  4  5  6  7  8  9  T
		protected double[] counts = new double[] {-2, 2, 2, 2, 3, 2, 1, 0,-1,-2 };
		new private string name = "Uston SS";
		new private int level = 2;
		public override string MethodName{ get{ return name; }}
		public override int MethodLevel{ get{ return level; }}
	}

	public class WongHalves : CountMethod
	{
		public WongHalves( int n ) : base( n )
		{
		}

		protected override double[] GetCounts() { return counts; }
		// Card counts                             A  2  3  4  5  6  7  8  9  T
		protected double[] counts = new double[] {-1,.5, 1, 1,1.5,1,.5, 0,-.5,-1 };
		new private string name = "Wong Halves";
		new private int level = 1;
		public override string MethodName{ get{ return name; }}
		public override int MethodLevel{ get{ return level; }}
	}

	public class ZenCount : CountMethod
	{
		public ZenCount( int n ) : base( n )
		{
		}

		protected override double[] GetCounts() { return counts; }
		// Card counts                             A  2  3  4  5  6  7  8  9  T
		protected double[] counts = new double[] {-1, 1, 1, 2, 2, 2, 1, 0, 0,-2 };
		new private string name = "Zen Count";
		new private int level = 2;
		public override string MethodName{ get{ return name; }}
		public override int MethodLevel{ get{ return level; }}
	}

	public class HiLoA : CountMethod
	{
		// The standard HiLo method with an Ace side count
		public HiLoA( int n ) : base( n )
		{
		}

		protected override double[] GetCounts() { return counts; }
		// Card counts                             A  2  3  4  5  6  7  8  9  T
		protected double[] counts = new double[] { 0, 0, 1, 1, 1, 1, 0, 0, 0,-1 };
		public override double GetWager( double normalBet )
		{
			double wager = 0;
			double trueCount = Count;
			int aceCount = base.SideCount( Card.CardType.Ace );

			double aceRatio = ((cardCount / 13.0) - aceCount) / (( Decks*52 - cardCount ) / 52.0 );
			trueCount += aceRatio;

			if( trueCount > 0 )
				wager = normalBet * trueCount;
			else if( trueCount == 0 )
				wager = normalBet;
			else if( trueCount < 0 )
				wager = normalBet * trueCount;

			// $10 table minimum :)  Also, round to nearest integer
			wager = (int)Math.Max( 10, wager );

			return wager;
		}

		new private string name = "Hi-Lo Ace Side Count";
		new private int level = 1;
		public override string MethodName{ get{ return name; }}
		public override int MethodLevel{ get{ return level; }}
	}

	public class HiOptIA7 : CountMethod
	{
		public HiOptIA7( int n ) : base( n )
		{
		}

		protected override double[] GetCounts() { return counts; }
		// Card counts                             A  2  3  4  5  6  7  8  9  T
		protected double[] counts = new double[] { 0, 0, 1, 1, 1, 1, 0, 0, 0,-1 };
		public override double GetWager( double normalBet )
		{
			double wager = 0;
			double trueCount = Count;
			int aceCount = base.SideCount( Card.CardType.Ace );
			int sevenCount = base.SideCount( Card.CardType.Seven );

			double aceRatio = ((cardCount / 13.0) - aceCount) / (( Decks*52 - cardCount ) / 52.0 );
			double sevenRatio = ((cardCount / 13.0) - sevenCount) / (( Decks*52 - cardCount ) / 52.0 );
			trueCount += aceRatio;
			trueCount += sevenRatio;

			if( trueCount > 0 )
				wager = normalBet * trueCount;
			else if( trueCount == 0 )
				wager = normalBet;
			else if( trueCount < 0 )
				wager = normalBet * trueCount;

			// $10 table minimum :)  Also, round to nearest integer
			wager = (int)Math.Max( 10, wager );

			return wager;
		}

		new private string name = "Hi Opt I Ace Seven side counts";
		new private int level = 1;
		public override string MethodName{ get{ return name; }}
		public override int MethodLevel{ get{ return level; }}
	}

}
