using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace BlackJack
{
	/// <summary>
	/// Summary description for Splash.
	/// </summary>
	public class Splash : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Timer tmrSplash;
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.Label lblVersion;
		private static bool fadeOut = false;

		public Splash()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
			this.SetStyle(ControlStyles.UserPaint, true);
			this.SetStyle(ControlStyles.DoubleBuffer, true);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Splash));
			this.tmrSplash = new System.Windows.Forms.Timer(this.components);
			this.lblVersion = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// tmrSplash
			// 
			this.tmrSplash.Enabled = true;
			this.tmrSplash.Interval = 3000;
			this.tmrSplash.Tick += new System.EventHandler(this.tmrSplash_Tick);
			// 
			// lblVersion
			// 
			this.lblVersion.BackColor = System.Drawing.Color.Transparent;
			this.lblVersion.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblVersion.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(64)), ((System.Byte)(64)), ((System.Byte)(64)));
			this.lblVersion.Location = new System.Drawing.Point(8, 309);
			this.lblVersion.Name = "lblVersion";
			this.lblVersion.Size = new System.Drawing.Size(216, 23);
			this.lblVersion.TabIndex = 0;
			this.lblVersion.Text = "version";
			// 
			// Splash
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
			this.BackgroundImage = ((System.Drawing.Bitmap)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(425, 326);
			this.ControlBox = false;
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.lblVersion});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "Splash";
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Blackjack";
			this.TopMost = true;
			this.Click += new System.EventHandler(this.Splash_Click);
			this.Load += new System.EventHandler(this.Splash_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void tmrSplash_Tick(object sender, System.EventArgs e)
		{
			if( fadeOut )
			{
				tmrSplash.Interval = 10;
				this.Opacity -= .02;
				if( this.Opacity == 0 )
				{
					tmrSplash.Enabled = false;
					Close();
				}
			}
			else
			{
				fadeOut = true;
			}
		}

		private void Splash_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void Splash_Load(object sender, System.EventArgs e)
		{
			lblVersion.Text = "version " + Application.ProductVersion.ToString();
		}
	}
}
