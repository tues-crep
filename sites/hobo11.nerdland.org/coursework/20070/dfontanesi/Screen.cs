using System;
using System.Drawing;
using System.Windows.Forms;

namespace BlackJack
{

	public class Screen
	{
		protected Graphics drawingSurface = null;
		protected Image offScreenBitmap = null;
		protected Graphics offScreenSurface = null;
		protected Rectangle screenSize;
		protected string backgroundImage;

		public enum InvalidationType
		{
			none = 0,
			labelOnly = 1,
			latestCard = 2,
			latestHand = 3,
			bothHands = 4,
			all = 5
		}
		
		public Screen( Form parent, string image )
		{
			// get a visible screen
			drawingSurface = parent.CreateGraphics();
			screenSize = parent.ClientRectangle;

			// get offscreen buffer context
			offScreenBitmap = new Bitmap( screenSize.Width, screenSize.Height );
			offScreenSurface = Graphics.FromImage( offScreenBitmap );

			backgroundImage = image;
		}

		public Graphics screenGraphics
		{
			get { return offScreenSurface; }
		}

		public void Erase()
		{
			// erase all content in back buffer by redrawing the background
			if ( drawingSurface != null && offScreenSurface != null )
				offScreenSurface.DrawImage( Image.FromFile( backgroundImage ), screenSize.X, screenSize.Y );
		}

		public void Flip()
		{
			// flip buffers for smooth animation
			drawingSurface.DrawImage( offScreenBitmap, screenSize.X, screenSize.Y );
		}
	}
}


