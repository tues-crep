using System;

namespace BlackJack
{
	/// <summary>
	/// Summary description for Deck.
	/// </summary>
	public class Deck
	{
		private const int numberCardsPerDeck = 52;
		private Card[] cards;

		public Deck()
		{
			for( int i=0; i < numberCardsPerDeck; i++ )
			{
				cards[i].cardValue = i;
				switch( i )
				{
					case 0:
						cards[i].face = "A diamonds";
						break;
					case 1:
						cards[i].face = "A clubs";
						break;
					case 2:
						cards[i].face = "A hearts";
						break;
					case 3:
						cards[i].face = "A spades";
						break;
					case 4:
						cards[i].face = "2 diamonds";
						break;
					case 5:
						cards[i].face = "2 clubs";
						break;
					case 6:
						cards[i].face = "2 hearts";
						break;
					case 7:
						cards[i].face = "2 spades";
						break;
					case 8:
						cards[i].face = "3 diamonds";
						break;
					case 9:
						cards[i].face = "3 clubs";
						break;
					case 10:
						cards[i].face = "3 hearts";
						break;
					case 11:
						cards[i].face = "3 spades";
						break;
					case 12:
						cards[i].face = "4 diamonds";
						break;
					case 13:
						cards[i].face = "4 clubs";
						break;
					case 14:
						cards[i].face = "4 hearts";
						break;
					case 15:
						cards[i].face = "4 spades";
						break;
				}
			}
		}
	}
}
