using System;

namespace BlackJack
{
    class Table {
	public Card[] dealerHand;
	public Card[] playerHand;
	public Deck deck;
    }

    /// <summary>
    /// This is a simple game to test Deck object inside of something
    /// a tad bit useful.  A simulation of two player blackjack with
    /// only one Deck should give us a decent idea of the statistics
    /// after each deck has been fully dealt.
    /// </summary>
    class SimpleTable : Table {

	public SimpleTable(Deck d) {
	    deck=d;
	    dealerHand=new Card[];
	    playerHand=new Card[];
	}

	/// <summary>
	/// Set up a SimpleTable with a Dealer and a Deck.
	/// Put the Dealer to work on shuffling, offering a cut,
	/// and finally dealing each hand and calling all of the
	/// winners.  Player should listen to the dealer and keep
	/// score to offer a report at the end of the deck.
	/// </summary>
	public static void Main() {
	    Console.WriteLine("--------------------");
	    Console.WriteLine("BlackJack Simulation");
	    Console.WriteLine("--------------------");

	    // Put the Deck on the Table
	    Deck c=new Deck();
	    Table t=new SimpleTable(c);
	    // Dealer walks up to the Table
	    Dealer d=new Dealer(t);

	    d.Shuffle();
	    //d.OfferCut();
	    while(c.CardsLeft) {
		bool winner=d.DealHand();
		if(winner) {
		    Console.WriteLine("Won a hand!");
		}
		else {
		    Console.WriteLine("Loser.");
		}
	    }
	    Console.WriteLine("--------------------");
	    Console.WriteLine("Player Report:");
	    Console.WriteLine();
	    Console.WriteLine("--------------------");
	}
    }

    /// <summary>
    /// Dealer gets a new deck, shuffles it, and drops some cards
    /// into the spaces on the table.  The dealer observes the cards,
    /// gives an option to the Player, and announces the values after
    /// the options are finished.  Dealer is responsible to call the
    /// winners and tell them about their good fortune, then clear the
    /// table and deal another hand. Money is not yet implemented, and
    /// the Player need not exist.
    /// </summary>
    class Dealer {
	public Table table;

	public Dealer(Table t) {
	    table=t;
	}

	public void Shuffle() {
	    table.deck.Shuffle();
	}

	public bool DealHand() {
	    // Deal two cards to the Dealer
	    table.dealerHand+=table.deck.NextCard.TrueValue;
	    table.dealerHand+=table.deck.NextCard.TrueValue;

	    // Deal two cards to the Player
	    table.playerHand+=table.deck.NextCard.TrueValue;
	    table.playerHand+=table.deck.NextCard.TrueValue;

	    // FIXME We should actually keep the Card objects to handle
	    // dualistic point values for Ace, and to contribute to the
	    // realism of the simulation.

	    return table.playerHand>table.dealerHand;    // Winner!
	}
    }
}
