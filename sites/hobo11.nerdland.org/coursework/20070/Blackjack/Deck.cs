using System;

namespace BlackJack
{
    class Deck:CardPile {

	public Deck() {
	    NumberOfDecks=1;
	    Init();
	}

	private int nextCard=0;
	public Card NextCard
	{
	    get
	    {
		Card next=cards[nextCard];
		nextCard++;
		return next;
	    }
	    // does not accept replacement cards, you'll have to
	    // get a new deck once this one is dealt out
	    /*
	    set
	    {
	    }
	    */
	}

	public bool CardsLeft 
	{
	    get
	    {
		if(nextCard==CARDSPERDECK*numberOfDecks) {
		    return false;
		}
		return true;
	    }
	}


	override public string ToString() {
	    string s="";
	    for(int i=0;i<cards.Length;i++) 
	    {
		s = s + cards[i].Value + ":";
		s = s + cards[i].Suit + "," + cards[i].FaceValue + "\n";
	    }
	    return s;
	}


	public static void Main () {
	    Deck myDeck=new Deck();

	    myDeck.Shuffle();

	    Console.Write(myDeck);
	}
    }
}
