using System;
using System.Reflection;
using System.Resources;
using System.Collections;
using System.Globalization;

namespace BlackJack
{
    /// <summary>
    /// Cards have identity that is composed of a suit and a value.  Cards
    /// also have a face point value which varies by game; in BlackJack,
    /// the Ace has two point values, 11 or 1.  All Face Cards have the
    /// same value as 10.
    /// </summary>
    public class Card : ICloneable
    {
	private CardType cardType;
	private Suits cardSuit;
	private int value;
	private int trueValue;

	public enum CardType
	{
	    Ace = 0,
	    Two = 1,
	    Three = 2,
	    Four = 3,
	    Five = 4,
	    Six = 5,
	    Seven = 6,
	    Eight = 7,
	    Nine = 8,
	    Ten = 9,
	    Jack = 10,
	    Queen = 11,
	    King = 12
	}

	public enum Suits 
	{
	    Clubs = 0,
	    Diamonds = 1,
	    Hearts = 2,
	    Spades = 3
	}

	/// <summary>
	/// Method to instantiate a new Card object.
	/// </summary>
	public Card( CardType type, Suits suit )
	{
	    cardSuit = suit;
	    cardType = type;
	    value = ((int)suit * 13) + (int)cardType + 1;

	    // Figure out how these Resources are loaded, and duplicate for Cocoa if necessary
	    //image = Resources.GetImage(value);
	    trueValue = (int)cardType;
	    if( trueValue > 9 )
		trueValue = 9;

	    //cardSize = image.PhysicalDimension;
	    //cardSpacing.Width = cardSize.Width / 5;
	    //cardSpacing.Height = cardSize.Height / 7;
	}

	/// <summary>
	/// This method was removed to make way for Cocoa.
	/// </summary>
	public void Draw( bool show, bool dim, bool doubledownCard )
	{
	    //			float opaqueness = dim ? .5F : 1;
	    //			float rotationAngle = doubledownCard ? 45 : 0;
	    //			float[][] ptsArray ={	new float[] {1, 0, 0, 0, 0},
	    //									new float[] {0, 1, 0, 0, 0},
	    //									new float[] {0, 0, 1, 0, 0},
	    //									new float[] {0, 0, 0, opaqueness, 0}, 
	    //									new float[] {0, 0, 0, 0, 1}};
	    //			ColorMatrix clrMatrix = new ColorMatrix(ptsArray);
	    //			ImageAttributes imgAttributes = new ImageAttributes();
	    //			imgAttributes.SetColorMatrix(clrMatrix,	ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
	    //
	    //			GraphicsState previousState = drawingSurface.Save();
	    //
	    //				Point Pcenter = new Point( location.X + image.Width / 2, location.Y + image.Height / 2 );
	    //				drawingSurface.TranslateTransform( Pcenter.X, Pcenter.Y );
	    //				drawingSurface.RotateTransform( rotationAngle );
	    //
	    //				//			drawingSurface.DrawImage(shadow, new Rectangle( -curImage.Width/2, -curImage.Height/2 , shadow.Width, shadow.Height ), 0, 0, shadow.Width, shadow.Height, GraphicsUnit.Pixel, imgAttributesShadow );
	    //				drawingSurface.DrawImage( show ? image : Shoe.BackImage, new Rectangle( -image.Width/2, -image.Height/2 , image.Width, image.Height ), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, imgAttributes );
	    //
	    //			drawingSurface.Restore( previousState );
	}
	public int Value 
	{
	    get { return value; }
	}

	public CardType FaceValue
	{
	    get { return cardType; }
	}

	public int TrueValue
	{
	    get { return trueValue; }
	}

	public Suits Suit 
	{
	    get { return cardSuit; }
	}	

	//	ICloneable
	Object ICloneable.Clone() 
	{
	    return new Card(cardType, cardSuit);
	}

	public Card Clone() 
	{
	    return new Card(cardType, cardSuit);
	}

	public static Card Clone(Card card) 
	{
	    return new Card(card.FaceValue, card.Suit);
	}
    }

    //	A shared reference to access images and other resources.
    internal abstract class Resources 
    {
	private static ResourceManager images;

	static Resources() 
	{
	    images = new ResourceManager("Blackjack.Images", Assembly.GetExecutingAssembly());
	}

	public static ResourceManager Images 
	{
	    get { return images; }
	}

	//We also lost Images
	//public static Image GetImage(int imageId) 
	//{
	//	return (Image)images.GetObject(imageId.ToString(CultureInfo.InvariantCulture), CultureInfo.InvariantCulture );
	//}

	//.. by name and int both!
	//public static Image GetImage(string imageId) 
	//{
	//	return (Image)images.GetObject(imageId, CultureInfo.InvariantCulture);
	//}
    } // Resources
}
