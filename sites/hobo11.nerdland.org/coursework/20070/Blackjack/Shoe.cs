using System;
using System.Collections;
using System.Globalization;

namespace BlackJack
{
	/// <summary>
	/// Summary description for Shoe.
	/// </summary>
	public class Shoe
	{
		private const int		CARDSPERDECK = 52;

		private Card[]			cards;
		private int			numberOfDecks = 5;
		private BackTypes		backType;
		private int			shoeLocation;

		//Integer values that correspond to the resource image names
		public enum BackTypes 
		{
			CrossHatch = 53,
			Weave1 = 54,
			Weave2 = 55,
			Robot = 56,
			Flowers = 57,
			Vine1 = 58,
			Vine2 = 59,
			Fish1 = 60,
			Fish2 = 61,
			Shells = 62,
			Castle = 63,
			Island = 64,
			Sleeve = 65,
			Bellagio = 66
		}

		// A delegate type for hooking up change notifications.
		public delegate void ShoeEventHandler(object sender, EventArgs e);
		
		public event ShoeEventHandler EndOfShoeEvent;
		public event ShoeEventHandler BackChangedEvent;
		public event ShoeEventHandler ShuffleEvent;

		public int NumberOfDecks 
		{
			get 
			{ 
				return numberOfDecks;	
			}
			set 
			{
				if (numberOfDecks != value) 
					numberOfDecks = value;
			}
		}

		public int ShoeLocation
		{
			get { return shoeLocation; }
		}

		public Shoe()
		{
			Init();
			// Set back type to Bellagio if none specified
			backType = BackTypes.Bellagio;
			//backImage = Resources.GetImage((int)backType);
		}

		//public static Image BackImage 
		//{
		//	get { return backImage; }
		//}

		public BackTypes CardBack 
		{
			get { return backType; }
			set 
			{ 
				backType = value;
				//backImage = Resources.GetImage(((int)value).ToString(CultureInfo.InvariantCulture));
				BackChangedEvent(this, EventArgs.Empty);
			}
		}

		public void Init()
		{
			shoeLocation = 0;

			cards = new Card[ numberOfDecks * CARDSPERDECK ];
			int current = 0;

			for( int j=0; j < numberOfDecks; j++ )
			{
				for ( int y = 0; y < 4; y++) 
				{
					for ( int x = 0; x < 13; x++) 
					{
						cards[current++] = new Card( (Card.CardType)x, (Card.Suits)y );			
					}
				}
			}
		}

		//	End-of-Deck property
		public bool Eod
		{
			get { return (shoeLocation >= cards.Length * .66); }
		}
		
		//	Beginning-of-Deck property
		public bool Bod 
		{
			get { return (shoeLocation == 0); }
		}

		public void Shuffle()
		{
			Random rand = new Random(unchecked((int)DateTime.Now.Ticks)); 

			for( int i = 0; i< rand.Next(10,100); i++ )
			{
				shoeLocation = 0;

				Card[] shuffledCards = new Card[ numberOfDecks * CARDSPERDECK ];
		
				Random index = new Random(unchecked((int)DateTime.Now.Ticks)); 
				int upperLimit = cards.GetUpperBound(0);

				for( int j = 0; j < numberOfDecks * CARDSPERDECK; j++ )
				{
					int k = index.Next(0, upperLimit);
					shuffledCards[j] = cards[k];
					cards[k] = cards[upperLimit--];
				}

				cards = shuffledCards;
			}

			ShuffleEvent(this, EventArgs.Empty );
		}

		//	Indexer
		public Card this[int index] 
		{	
			get 
			{
				if (index < 0 || index > 52)
					throw new ArgumentOutOfRangeException();
				else
					return cards[index];
			}
		}

		public Card Next()
		{
			shoeLocation++;

			if( shoeLocation < cards.GetUpperBound(0) )
			{
				if( Eod )
					EndOfShoeEvent(this, EventArgs.Empty );

				return cards[shoeLocation];
			}
			else
			{
				// It shouldn't come to this, but in the unlikely event that we
				// run out of cards, reshuffle the shoe.
				Shuffle();
				return cards[shoeLocation];
			}
		}

		// Designed to be used by Select Back forms
		public static Hashtable GetAllBackImages() 
		{
			Hashtable images = new Hashtable(12);

			for (int x = 53; x < 66; x++) 
			{
				//images.Add(x, Resources.GetImage(x));
			}

			return images;
		}

	}
}
