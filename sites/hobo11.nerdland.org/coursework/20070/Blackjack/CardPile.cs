using System;
using System.Collections;
using System.Globalization;

namespace BlackJack
{
    /// <summary>
    /// CardArray is a simple type for CardPile to inherit from, and to
    /// help clarify in Shuffle how to manage the process of merging
    /// several ArrayList objects with the same inner type.
    /// </summary>
    public class CardArray
    {
	protected ArrayList cards=new ArrayList();

	// Draw the top card as a property of the CardPile.  Note that this
	// will remove the card from the CardPile!  Put it into another pile
	// somewhere or you might as well just go ahead and burn it for the
	// garbage collector.  Cards are only added to the bottom of a pile
	// (That's queue structure, not stack structure.)  So for the set
	// method, technically TopCard is a total misnomer.
	public Card TopCard
	{	
	    get 
	    {
		if (cards.Count < 1)
		    throw new IndexOutOfRangeException();
		else
		    return cards.RemoveAt(0);
	    }
	    set
	    {
		cards.Add(value);
	    }
	}

    }

    /// <summary>
    /// CardPile is not as dumb as it looks.  A CardPile is configured by
    /// default as a Blackjack Shoe with 6 decks.  Included methods allow
    /// a Dealer to clear the Shoe completely of cards, to remove a Card
    /// from the top of the pile, or to add Card or CardArray objects at
    /// the bottom.
    /// FIXME - No support for modeling cards as face-up and face-down.
    /// </summary>
    public class CardPile : CardArray
    {
	protected const int CARDSPERDECK = 52;
	protected int numberOfDecks = 6;
	// The NumberOfDecks property determines the size and contents
	// of the ArrayList cards, and the number of piles involved in
	// Shuffle.
	public int NumberOfDecks 
	{
	    get 
	    { 
		return numberOfDecks;	
	    }
	    set 
	    {
		if (numberOfDecks != value) 
		    numberOfDecks = value;
	    }
	}

	// Fill up our CardPile with cards equivalent to NumberOfDecks*52
	// Card objects.  Note that there are only 52 unique cards no matter
	// how many Decks are used.
	public CardPile()
	{
	    Init();
	}

	// Include for each deck four suits of 13 different cards
	public void Init()
	{
	    for( int j=0; j < numberOfDecks; j++ )
	    {
		for ( int y = 0; y < 4; y++) 
		{
		    for ( int x = 0; x < 13; x++) 
		    {
			TopCard=new Card((Card.CardType)x, (Card.Suits)y);
		    }
		}
	    }
	}

	// The trick is to randomly spray the cards into some number of
	// slots greater than the number of cards, and then collect those
	// piles in order again.
	public void Shuffle() {
	    ArrayList pile=new ArrayList(cards.Count*6);

	    Random rnd = new Random(unchecked((int)DateTime.Now.Ticks));
	    Byte[] b = new Byte[10];
	    rnd.NextBytes(b);

	    // shuffle from cards to pile
	    foreach(Card c in cards)
	    {
	    }
	    for(int index=0; index<CARDSPERDECK*numberOfDecks; index++) {

		// pick random b's in order from the b generator
		Byte newHome=b[index%10];

		// throw them into the first empty space that the
		// almighty random number points to
		while(pile[newHome]!=null) {
		    newHome++;
		}
		pile[newHome]=cards[index];
	    }

	    // order pile back into cards[52]
	    int card=0;
	    for(int index=0; index<256; index++) {
		while(index<256 && pile[index]==null) index++;
		if(index<256 && pile[index]!=null)
		{
		    cards[card]=pile[index];
		    card++;
		}
	    }

	    /* Random from Microsoft
	     * http://msdn2/en-us/library/system.random.nextbytes.aspx
	    Console.WriteLine("The Random bytes are: ");
	    for (int i = 0; i < 10; i++) {
		Console.Write(i);  
		Console.Write(":");
		Console.WriteLine(b[i]);
	    }*/

	}

    }
}
