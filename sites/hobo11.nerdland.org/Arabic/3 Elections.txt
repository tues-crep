﻿انتخاب، انتخابات
election
انتخابات فرعية
by-elections
انتخابات عامة
general elections
انتخابات رئاسية
presidential elections
انتخابات مبكرة
early elections
استطلاع رأي
opinion poll
اقتراع ثقة
vote of confidence
حملة، حملات
campaign
مرشح مستقل
independent candidate
شخصية، شخصيات
personality
صورة، صور
image
شهرة
reputation
اشتهار
notoriety
شعبية
popularity
تراجع في الشعبية
a decline in popularity
أحزان متصارعة
contending parties
ولاءل
loyalty to
غير الناخبين
non-voters
مسجلون في قوائم الاقتراع
registered voters
الناخبون
the electorate
مقعد، مقاعد
seat
صوت أصوات
vote
انتصار، انتصارات
victory
تزوير الانتخابات
election-rigging
انتخابات حرة ونظيفة
free and fair elections
الحزب الأكثر اتساعاً
the most widespread party
الحزب الأسرع نمواً
the fastest-growing party
دعاية
propaganda
أجرى انتخابات
to hold elections
أجل انتخابات
to delay elections
ألغى انتخابات
to cancel elections
صوت
to vote
أحجم عن
to abstain from
فاز يفوز فوز في
to gain, win votes
أحرز انتصاراً ساحقاً
to win a landslide victory
أحرز أغلبية ساحقة
to secure an overwhelming majority
أخذ يأخذ أخذ أعنة الحكومة
to take up the reins of government
وصل يصل وصول إلى السلطة
to come to power
رشح
to appoint
نصب
to appoint