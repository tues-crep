﻿المؤسسة العسكرية
the military
العسكريون
the military
جيش، جيوش
army
القوات المسلحة
the armed forces
العوات
the troops
ركن، أركان
soldier
مندي، جنود
civilian
المشاة
the infantry
بحار، بحارون/بجارة
sailor
جندي مشاة البحرية
marine
طيار، طيارون
airman, pilot
مظلي، مظليون
paratrooper
قانص، قناص
sniper
بطل، أبطال
hero
حليف، حلفاء
ally, confederate
لواء، ألوية
general (rank); flag
رئيس الأركان
chief of staff
رتبة، رتب
rank, grade
قوة جوية
air force
قوات برية
ground forces
قوات احتياطية
reserves
قوة خاصة
commando, special troop
قوة خفيفة
light/mobile task force
قطعات قتالية
combat troops
قوات حفظ السلام
peacekeeping forces
قوات التحالف
the Allied Forces
قوة هجومية ضاربة
strike force
قوة ردع
deterrent force
فصيلة، فصائل
cell, squad
فصيلة الإعدام
execution squad, firing squad
دورية، دوريات
patrol
وحدة، وحدات
(military) unit
سرب، أسراب
squadron
كتيبة، كتائب
battalion
سلام، أسلحة
weapon
أسلحة خفيفة/صغيرة
small arms
ذخيرة
ammunition
مظلة، مظلات
parachute
ترسانة، ترسانات
arsenal
أسلحة نووية
nuclear weapons
أسلحة ذكية
precision weapons
أسلحة الدمار الشامل
weapons of mass destruction (WMD)
صاروخ، صواريخ
missile, rocket
صاروخ مضاد للطائرات
anti-aircraft missile
صاروخ تمويهي
decoy missile
صاروخ طواف
cruise missile
صاروخ سطح - جو
surface-to-air missile
صاروخ بعيد/قصير المدى
long-/short-range missile
قذيفة عابرة للقارات
intercontinental ballistic missile
قاذفة الصواريخ
rocket/missile launcher
نسيفة، نسائف
torpedo
قنبلة، قنابل
bomb
أداة تفجير
explosive device
متفجرات
explosives
قنبلة يدوية
hand grenade
قنبلة نووية
nuclear bomb
قنبلة عنقودية
cluster bomb
قنبلة حارقة
incendiary bomb
قذيفة هاون، قذائف هاون
mortar bomb
لغم ألغام
mine
رصاصة، رصاصات/رصاص
bullet
مدفع رشاش
machine gun
بندقية آلية
automatic rifle
سلاح قذافي
ballistic weapon
قطعة مدفعية
(piece of) artillery
قصف مدفعي
artillery fire
قصف صاروخي
rocket fire
شظايا
shrapnel
إشعاع
radiation
انفجار/تفجير/تفجر
explosion
عصف
blast
قصف
bombardment, shelling
قصف دقيق/ذكي
precision bombing
ضربة وقائية
preemptive strike
استخدام القوة
use of force
غطاء جوي
air cover
منطقة حظر جوي
no-fly zone
ناقلة جنود مدرعة
armoured personnel carrier
دبابة، دبابات
tank
جرافة، جرافات
bulldozer
طائرة عمودية
helicopter
طائرة مروحية
helicopter
هليكوبتر
helicopter
غواصة مضادة للطائرات
anti-aircraft submarine
حاملة طائرات
aircraft carrier
طائرة مقاتلة
fighter aircraft
مدمرة، مدمرات
destroyer
سفينة حربية، سفن حربية
warship, frigate
بارجة، بوارج
warship, frigate
زورق حربي، زوارق حربية
gunboat
نسافة، نسافات
torpedo boat
أسطول، أساطيل
fleet
قاعدة عسكرية، قواعد عسكرية
military base
مقر، مقار
headquarters (HQ)
موقع استراتجي، مواقع استراتيجية
strategic point
منطقة القتال، مناطق القتال
combat zone
منطقة حالية من الأسلحة النووية
nuclear-free zone
انتشار الأسلحة النووية
nuclear proliferation
جهاز طرد مركزي
centrifugal system
تخصيب اليورانيوم
enrichment of uranium
حرب، حروب
war
حرب أهلية
civil war
حرب عصابات
guerilla warfare
في حالة الحرب
at war
مسرح الحرب
theatre of war
ميدان العمليات
field of operations
مناورة، مناورات
manoeuvre
استطلاع
reconnaissance
استعراض/عرض
parade
معركة، معارك
battle
خطر، أخطار/مخاطر
danger
أمن
safety
وقف إطلاق النار
ceasefire
تجريد من السلاح
disarmament; demilitarisation
أعمال العنف
acts of violence
أعمال التخريب
acts of sabotage
علمية انتحارية
suicide bombing
سيارة مفخخة
car bomb (lit. booby-trapped car)
مجزرة، مجازر
massacre
مصرع، مصارع
death; battleground
مقتل
murder, killing
قطع الرأس
beheading
تدخل عسكري
military intervention
إصابة، إصابات
injury
جرح، جروح/جراح
injury
جنازة، جنازات/جنائز
funeral (procession)
ضحية، ضحايا
victim
جريح، جرحى
wounded, injured (person)
قتيل، قتلى
dead (person)
أسير، أسرى
captured (person), prisoner
محتجز، محتجزون
detainee
منفذ عملية انتحارية
suicide bomber
مباغتة، مباغتات
surprise attack, raid
هجوم بري
ground offensive, land attack
هجوم برمائي
amphibious attack
هجوم إرهابي
terrorist attack
هجوم مسلح على
armed attack on
هجوم مضاد
counter attack
كمين
ambush
تكتيكات
tactics
اعتداءات على
aggressions against
رد انتقامي على
revenge for
على متن
on board (a ship/aircraft)
في وضع استعداد ل
in a state of readiness for
منتشر
deployed (lit. spreading out)
عدواني/عواقب
repercussions
حشد يحشد حشد
to mobilize, call up, to accumulate
عبأ
to mobilize
قام بتعبئة
to mobilize
حشد
to amass (especially troops)
جند
to draft, conscript, mobilize
استدعى
to call up
عزز القوات
to reinforce the troops
نشر ينشر نشر
to deploy
نفقد
to inspect
فجر
to explode s.t.
تفجر
to explode (intransitive)
دمر
to destroy
هدم يهدم هدم
to destroy
أباد
to annihilate
أفنى
to annihilate
تبادل إطلاق النار
to exchange fire
أودى بحياة
to claim the life/lives of
استشهد
to be martyred
أصيب بجروح خطيرة/طفيفة
to suffer serious/minor injuries
هجم يهجم هجوم على
to attack
غزا يغزو غزو
to attack, invade, raid
شن يشن شن هجوماً/حملةً
to launch an attack/a campaign
أطلق
to launch (missile, torpedo, etc.)
اقتحم
to storm
تسلل إلى
to infiltrate
توغل في
to advance further, to penetrate deeply into
حاصر
to besiege, surround
فتح يفتح فتح النار
to open fire
أطلق النار
to open fire
دافع عن
to defend
حمى يحمي حماية ه من
to protect s.t. against
انسحب
to withdraw (intransitive)
جلا يجلو جلاء عن
to evacuate, pull out of
أجلى
to evacuate (transitive)
استسلم
to surrender
احتجز
to detain
أفرج عن
to release
أطلق
to release
أطلق سراحه
to set s.o. free
أخلى سبيله
to let s.o. go
استأنف القتال
to resume fighting
قاوم
to resist
خطط
to plan
تآمر
to plot
استهدف تحديداً
to target specifically
نصب ينصب نصب حواجز على الطرق
to erect road blocks
فرض يفرض فرض حصاراً على
to impose a cordon/blocade on
اندلع
to break out (war)
بوسعه أن
to be in s.o.'s power to
احتل
to occupy
أحل محله
to take the place of
اخترق الحدود
to cross the border(s)
قدر حجم القوات ب
to estimate the strength of forces at
استخلص المعلومات
to debrief
