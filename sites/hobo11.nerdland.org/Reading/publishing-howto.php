<html>
<head>
<title>Driving Traffic</title>
</head>
<body>

<h3> Driving Traffic: An Internet Publisher's HOWTO </h3>

<h4> Getting Started: Publish an Article </h4>

<p>The first step to generating a buzz is to publish some content where people can see it.  Publishing on the Internet requires a web server of some kind.  A server is often just a regular computer, except its users are generally sitting in front of a monitor somewhere on the other side of a network.</p>

<p>There are plenty of free servers and web hosting services that you can use to get yourself started generating a buzz, and if you are just getting started with web publishing, then this is probably your best bet.  For a simple blog-type journal site, I recommend Google's free <a href="http://www.blogspot.com">Blogger</a> service.  This has the advantage that no servers of your own are required; you can sign up for an account and begin publishing articles immediately.</p>

<p>There are large and small communities of bloggers, and it's possible to stumble directly from one Blogger blog to the next, but this is not the most efficient way to drive traffic to your site.  Odds are that interesting people will not "randomly" find your blog unless you contact a publisher of a similar blog, and ask them to review your articles.  If they like what you have to say, they will usually be happy to post a link, and their readers may soon become your readers too!</p>

<h4> Pushing the Envelope: Mass Marketing </h4>

<p>You have a contact list full of friends who probably share many of your interests, and you've told them all about your blog... well it's great to have a group of bloggers communicating and supporting each other.  This can be very helpful in a workgroup setting.  Sometimes a mailing list works just as well.  Some bloggers like blogs because the audience can scale up and scale down in a moment; it's easy for a message to get lost in a high-traffic mailing list, but a blog article has a URL and perma-linking is becoming more popular by the day.</p>

<p>Hosting providers enable publishers and editors to post their own works.  Take for example:</p>
<ul>
<li><a href="http://slashdot.org/">Slashdot</a></li>
<li><a href="http://kuro5hin.org/">Kuro5hin</a></li>
<li><a href="http://digg.com/">Digg</a></li>
<li><a href="http://stumbleupon.com">StumbleUpon</a></li>
</ul>
<p>These sites all go about promoting web content in different ways: for a marketroid, responsibility boils down to maintaining a count of readers and regular subscribers.  For a computer scientist, this is not so easy; how can you begin to guess how many callers you've had if they don't leave their name and number with each message?  With no further analysis, advertising will sometimes provoke a response, and market shapes and sizes can be determined based on response data using comparative means.</p>

<h4> Building an Audience </h4>
<p>
The first tool for calculating the size of an audience is <a href="http://kingdon.nerdland.org/Reading/howto-sharing-bandwidth-and-disk.html">a Digg counter</a>.  These widgets enable readers who acquire Digg credentials to attach a discussion forum to any web URL; there is an initial reviewer who is the first to "digg" a URL, and every comment after that is either an addition or a derivative work of the information prior.</p>
<p>A Digg counter widget serves to attach a discussion forum to any URL in this way; this functionality overlaps with a blog's built in comment functionality, and this is another chance to add to the discussion of a creative work.</p>
<p>StumbleUpon is like Digg with a different approach to promoting reviewed sites.  Add <a href="http://www.stumbleupon.com">this toolbar</a> to your browser of choice for a similar ability to review a URL and provide a rating of thumbs-up, thumbs-down.  All of this is possible in the public eye today without any necessity for private server hosting!  NB: Every published content on the Internet has a URL or uniform resource locator.  This is similar to an address; content is not guaranteed to be unique, unchanging, or canonical for every web address, and this is the nature of the world and the Internet.  So just because a URL responds with a certain content today does not mean that this content will still be accessible at that URL tomorrow.  Take a copy, make a print!</p>
</p>
<h4> Monetization Strategies: Merchandising and Sponsorship </h4>
<p>Google has a product for that!  Try out <a href="https://www.google.com/adsense/">Google AdSense</a> for direct monetization strategies.</p>
<h4> Building a Team: Networking through Social Bookmarking </h4>
<p>TODO: Explain the difference between Digg, StumbleUpon, and <a href="http://del.icio.us/">http://del.icio.us</a>, and demonstrate the obsoletion of the monkey with typewriter.</p>

<hr/>

<iframe src="publishing-howto.html" frameborder="0" height="115" width="100" style="float: left">
<br/>
</iframe>
<p>The modern spam-filtering human receives his news using RSS technology from reliable and trusted sources.  Use my un-patented system for publishing and promotion, and start building your audience today!</p>
<p>I'm an independent publisher, and I'm always open to meeting people.  Thanks!  Yours truly, Kingdon.</p>

<table style="clear: left" border=0 cellpadding=0 cellspacing=0 width="100%">

<tr>
<td colspan=2 style="border-top: 1px solid #CCC; padding: 10px">
    <table border=0 cellpadding=0 cellspacing=0 width="100%"
	style="font: 11px arial,sans-serif">
    <tr>
    <td>
	<b style="font-size: 14px">Kingdon Barrett</b><br>
	<span style="font-size: 11px">RIT Research Team</span>
    </td>
    <td align=right> </td>
    </tr>
    </table>
</td>
</tr>

<tr>
<td colspan=2 style="padding: 0 0 10px 45px"> 
    <table border=0 cellpadding=0 cellspacing=0 width="100%" style="font: 11px arial,sans-serif">  
    <tr valign=top style="line-height: 15px">   
    <td width="33%">
	<b style="color: #999">Work:</b> +1(585)239-6035<br>
	<b style="color: #999">Mobile:</b> +1(585)781-4145<br> 
	<b style="color: #999">Fax:</b> +1(585)239-6020 
    </td>
    <td width="33%" style="padding-left: 15px">   
	<b style="color: #999">Email:</b> <a target="nw" href="mailto:kingdon@tuesdaystudios.com" style="text-decoration: none; color: #000">kingdon@tuesdaystudios.com</a><br> 
	<b style="color: #999">IM:</b> kingdon@tuesdaystudios.com (MSN)<br>
	
	<div style="margin-top: 5px">
	<b><a target="nw" href="http://www.linkedin.com/pub/2/a/731">Professional Profile</a></b><br>
	<b><a target="nw" href="http://sixthlayer.blogspot.com">The Sixth Layer</a></b>
	</div>
    </td>
    <td style="padding-left: 15px"></td>
    <td width="33%" style="padding: 0 15px">
	<b>Rochester Institute of Technology</b><br>
	<a target="nw" href="http://maps.google.com/maps?q=125+Tech+Park+Drive%2CSuite+1102%2CRochester%2CNY+14623%2CUnited+States+of+America&hl=en"  style="text-decoration: none; color: #000">125 Tech Park Drive<br>Suite 1102<br></a>
	Rochester, NY 14623<br>
	United States of America<br>
    </td>
    </tr>
    </table>
</td>
</tr>
<tr style="font: 10px arial,sans-serif">
<td style="border-top: 1px solid #CCC; padding-top: 2px"><a target="nw" href="http://kingdon.nerdland.org/">Visit Kingdon's Home Page</a></td>
<td align=right style="border-top: 1px solid #CCC; padding-top: 2px">Copyright &copy; 2007 Kingdon Barrett</td>
</tr>
</table>
