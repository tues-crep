<?php

// HTML Resume Template in PHP
// version 2.2, 2006-08-06
//
// Copyright (c) 2002-2006 Alex King
// http://www.alexking.org/software/resume/
//
// **********************************************************************
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// http://www.gnu.org/copyleft/gpl.html
//
// *****************************************************************
//

/* -- INITIALIZE DATA ------------------------------------------------------------ */

// we want to see any errors so we can fix them

ini_set('display_errors', '1');
ini_set('error_reporting', E_ALL);
error_reporting(E_ALL); 

$bio = array();
$data = array();
$data['jobs'] = array();
$prefs = array();

/* -- ENTER DATA ----------------------------------------------------------------- */

// start editing here

$bio['name'] 	= "Kingdon Patrick Barrett";
$bio['phone'] 	= "(585) 239-6035";
$bio['email'] 	= "kingdon@tuesdaystudios.com";
$bio['address']	= "125 Tech Park Drive STE 1102";
$bio['city']	= "Rochester";
$bio['state']	= "NY";
$bio['zip']	= "14623";

// set to 1 to show this info on the page, 0 to hide it

$prefs['show_phone']	= 1;
$prefs['show_email']	= 1;
$prefs['show_address']	= 1;

// if you want to be able to enter HTML (links, etc.) in your descriptions, set this to 1. This means you need to manually escape any content as HTML.

$prefs['encode_output']	= 1;

// enter job data here

$data['objective'] = "To connect with people and organizations that will benefit from my skills, to broaden my own base of experience, and to deliver prompt, reliable service meeting and exceeding the high standards that a business environment demands.";

// add more to this array as needed
$data['skills_technologies_projects'] = array(

"Internet TV and Telephony, Blogging, Web 2.0"
,"Storage Area Networks and Data Retention"
,"High Availability and Server Clustering"
,"Social Networking and Language Acquisition"

	);

// add more to this array as needed
$data['education_interests'] = array(

"Rochester Institute of Technology, degree anticipated in May 2008, BS Computer Science"
,"36 credits (minor) in Arabic Language, 12 credits (concentration) in Writing Studies"
,"President and Founder, Tuesday Studios at Venture Creations Business Development Lab"
,"Interactive Collaborative Technologies Specialist, RIT Lab for Applied Computing/CASCI"
,"Computer Science House, Phi Delta Theta, Empty Sky Go Club, Korean Student Association"
,"Tutor and Teaching Assistant, Golisano College of Computing and Information Sciences"
	);

// this is where you put all your job information
// add more of these arrays as needed

$data['jobs'][] = array(

'period' =>	"2005-Present"
,'employer' =>	"Venture Creations Business Development Lab"
,'location' =>	"Rochester, NY"
,'employer_info' =>	"Tuesday Studios builds and markets innovative middleware solutions."
,'employer_link' =>	"http://www.venturecreations.org/"
,'positions' => array('President and Founder, Network Admin' => array(

"Establish a secure and reliable distributed computing system to operate inside and across hostile network environments."
,"Catalog, quantify, establish blame, and resolve issues relating to intermittency in network services, from internal sources as well as upstream providers."
,"Design, build and maintain a top-shelf data center on a shoestring budget, minding high bandwidth applications as well as least-common-denominator and remote access."
,"Satisfy inter-departmental research goals with variable requirements, and do my best to keep pace with the research goals of the University."
		)
	    )
	,'show' => 0 // set to 0 to hide this job, 1 to show it
	);

$data['jobs'][] = array(

'period' =>	"2004-2007"
,'employer' =>	"Rochester Institute of Technology"
,'location' =>	"Rochester, NY"
,'employer_info' =>	"The primary objective of the CASCI is to create faculty/student and industry/government ventures to study and implement future CyberInfrastructure technologies, for solving computing problems in focused domains where we have renowned local expertise in the Institute."
,'employer_link' =>	"http://www.rit.edu/~rc/"
,'positions' => array('Collaboration Technologies Specialist' => array(

"Work with computers to enable communication between people across all kinds of technical borders, supporting the free exchange of information and ideas."
,"Explore collaborative technologies like Wikis, Internet TV and IP Telephony, and become familiar with technologies behind the so-called Web 2.0 revolution."
,"Connect directly with RIT faculty and administration and explore creative and independent problem solving techniques in my capacity as a researcher."

		),
	'Lab Assistant' => array(
"Responsible for timely attendance of tutoring office hours and labs."
,"Support computing resources in several labs and troubleshoot issues between inter-dependent systems."
,"Facilitate student programming activities and explain difficult course materials to enhance group learning."
		)
	    )
	,'show' => 0 // set to 0 to hide this job, 1 to show it
	);

$data['jobs'][] = array(

'period' =>	"2005-2006"
,'employer' =>	"American Fiber Systems"
,'location' =>	"Rochester, NY"
,'employer_info' =>	"AFS builds, owns, and maintains fiber networks in nine major metropolitan areas across the United States of America.  Business operations are homed here in Rochester, NY at the main office in the Meridian Centre Office Park off Winton Avenue."
,'employer_link' =>	"http://www.afsnetworks.com/"
,'positions' => array('IT Associate' => array(

"Built internal-use-only web application using Apache and PHP, integrating with a pre-existing Active Directory environment.",
"Testing and deployment of IBM servers, maintenance of previously deployed Sun and Windows servers.",
"Migration of proprietary NEC PBX system to Cisco IP Phones with Linux/Asterisk Open Source Telephony Platform."
		)
	    )
	,'show' => 1 // set to 0 to hide this job, 1 to show it
	);

$data['jobs'][] = array(
'period' =>	"Summer 2005"
,'employer' =>	"Robinson Home Products"
,'location' =>	"Buffalo, NY"
,'employer_info' =>	"Robinson Home Products markets quality kitchen utensils and food preparation tools.  Robinson evolved from a manufacturer of cutlery, kitchen tools, scissors, and fine edged industrial components."
,'employer_link' =>	"http://www.robinsonknife.com/"
,'positions' => array( 
    'Summer Internship' => array(
	"Inventory management, packing and shipping for Wal-Mart and other customers, and new employee orientation.",
	"Handled integration issues for SuSE Enterprise Linux, Novell Groupwise and Blackberry Enterprise Server."
	) )
,'show' => 1
	);

$data['jobs'][] = array(
'period' =>	"2003-2004"
,'employer' =>	"Financial Institutions, Inc."
,'location' =>	"Warsaw, NY"
,'employer_info' =>	"Financial Institutions, Inc. is a bank-holding Company that provides consumer, commercial and agricultural banking services through our bank and a non-bank subsidiaries."
,'employer_link' =>	"http://www.fiiwarsaw.com/"
,'positions' => array( 
    'Intranet Developer' => array(
	"Ground-up implementation of XML-based Intranet workflow and form processing software on a Microsoft IIS environment.",
	"Planned and started development of automated B2B/EDI services for integration of office supply chain with StaplesLink.com."
    ) )
,'show' => 1
	);

/* -- DONE ENTERING DATA - you shouldn't need to edit below this line ------------ */

/* ------------------------------------------------------------------------------- */

/* -- OUTPUT --------------------------------------------------------------------- */

function pad_and_wrap($string, $pad_to = 20, $pad_with = ' ', $wrap_at = 80, $bullet = '') {
    global $prefs;
    if (!empty($prefs['encode_output']) && $prefs['encode_output'] == 1) {
	$string = strip_tags($string);
    }
    if (!empty($bullet)) {
	$string = $bullet.$string;
    }
    if (strlen($string) > $wrap_at) {
	$wrap_line_at = strrpos(substr($string, 0, $wrap_at), ' ');
	$output = str_pad('', $pad_to, $pad_with).substr($string, 0, $wrap_line_at);
	$remainder = substr($string, $wrap_line_at + 1);
	if (strlen($remainder) > 0) {
	    $output .= "\n".pad_and_wrap($remainder, ($pad_to + strlen($bullet)), $pad_with, ($wrap_at - strlen($bullet)), '');
	}
    }
    else {
	$output = str_pad('', $pad_to, $pad_with).$string;
    }
    return $output;
}

function encode_output($str) {
    global $prefs;
    if (!empty($prefs['encode_output']) && $prefs['encode_output'] == 1) {
	return htmlentities($str);
    }
    else {
	return $str;
    }
}

// CSS

ob_start();
?>
body {
margin: 10px;
}
p, td, span, li, a { 
    font-family: verdana, arial, helvetica, sans-serif; 
}
h1 {
font: 90% verdana, arial, helvetica, sans-serif;
      font-weight: bold;
margin: 0 0 5px 0;
}
h2 {
    border-bottom: 1px solid #666;
clear: both;
font: 80% verdana, arial, helvetica, sans-serif;
      font-weight: bold;
margin: 15px 0 10px 0;
padding: 0 0 3px 0;
width: 700px;
}
h3 {
float: left;
font: 80% verdana, arial, helvetica, sans-serif;
      font-weight: bold;
margin: 0 0 5px 0;
padding: 0;
width: 446px;
}
li {
    font-size: 70%;
margin: 0 0 3px 20px;
padding: 0;
}
p {
    font-size: 70%;
margin: 0 0 10px 0;
padding: 0;
width: 575px;
}
body ul {
margin: 0 0 0 125px;
padding: 0;
width: 575px;
}
body div ul {
margin: 3px 0 0 0;
padding: 0;
width: 575px;
}
#bio_left {
    font-size: 75%;
float: left;
width: 350px;
}
#bio_right {
    font-size: 75%;
float: left;
       text-align: right;
width: 350px;
}
.company {
clear: both;
margin: 0 0 5px 0;
padding: 0;
}
.data {
    padding-left: 125px;
}
.date {
clear: left;
float: left;
       padding-top: 2px;
width: 125px;
}
.job {
clear: both;
width: 700px;
}
.job_data {
float: left;
width: 575px;
}
.location {
clear: right;
float: left;
       text-align: right;
width: 125px;
}
.position {
    font-style: italic;
margin: 0 0 5px 0;
}
#references {
    margin-top: 20px;
}
#meta {
    margin-top: 30px;
}
<?php
$css_screen = ob_get_contents();
ob_end_clean();

ob_start();
?>
body {
margin: 0px;
}
p, td, span, li, a { 
    font-family: verdana, arial, helvetica, sans-serif; 
}
h1 {
font: 14pt verdana, arial, helvetica, sans-serif;
      font-weight: bold;
margin: 0 0 5px 0;
}
h2 {
    border-bottom: 1px solid #666;
clear: both;
font: 11pt verdana, arial, helvetica, sans-serif;
      font-weight: bold;
margin: 15px 0 10px 0;
padding: 0 0 3px 0;
width: 100%;
}
h3 {
font: 11pt verdana, arial, helvetica, sans-serif;
      font-weight: bold;
margin: 0 0 2px 0;
padding: 0;
}
li {
    font-size: 10pt;
margin: 0 0 3px 5%;
padding: 0;
}
p {
    font-size: 10pt;
margin: 0 0 10px 0;
padding: 0;
}
body ul {
margin: 0 0 0 20%;
padding: 0;
}
body div ul {
margin: 3px 0 0 0;
padding: 0;
}
#bio_left {
    font-size: 11pt;
float: left;
width: 50%;
}
#bio_right {
    font-size: 11pt;
float: left;
       text-align: right;
width: 50%;
}
.company {
clear: both;
margin: 0 0 5px 0;
padding: 0;
}
.data {
    padding-left: 20%;
}
.date {
clear: left;
float: left;
       padding-top: 2px;
width: 20%;
}
.job {
clear: both;
width: 100%;
}
.job_data {
float: left;
width: 80%;
}
.location {
clear: right;
float: right;
       text-align: right;
width: 20%;
}
.position {
    font-style: italic;
margin: 0 0 5px 0;
}
#references {
    margin-top: 20px;
}
#meta {
display: none;
}
<?php
$css_print = ob_get_contents();
ob_end_clean();

ob_start();
$split_length = floor(strlen($bio['email']) / 4);
?>
function email() { // try to avoid spam trollers, intentionally complex
    var a = "<?php print(substr($bio['email'], 0, $split_length)); ?>";
    var b = "<?php print(substr($bio['email'], $split_length, $split_length)); ?>";
    var c = "<?php print(substr($bio['email'], ($split_length * 2), $split_length)); ?>";
    var d = "<?php print(substr($bio['email'], ($split_length * 3), ($split_length + (strlen($bio['email']) % 4)))); ?>";

    e_string = "<a href=\"ma" + "ilto:" + a + b + c + d + "\">" + a + b + c + d + "</a>";
    document.write(e_string);
}
<?php
$js = ob_get_contents();
ob_end_clean();

if (isset($_REQUEST["output"])) {
    $output = stripslashes($_REQUEST["output"]);
}
else {
    $output = 'html';
}
switch ($output) {
    case "css_screen":
	header("Content-type: text/css");
    print($css_screen);
    break;
    case "css_print":
	header("Content-type: text/css");
    print($css_print);
    break;
    case "js":
	header("Content-type: text/javascript");
    print($js);
    break;
    case "html":
	if (isset($_GET['contained']) && $_GET['contained'] == 1) {
	    header("Content-Type: application/force-download");
	    header("Content-Type: application/octet-stream");
	    header("Content-Type: application/download");
	    header("Content-Disposition: attachment; filename=\"resume.html\"");
	}
    ?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
	<!-- HTML Resume Template from http://www.alexking.org/software/resume/ -->
	<html>
	<head>
	<title><?php print(encode_output($bio['name'])); ?></title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<?php
	if (isset($_GET['contained']) && $_GET['contained'] == 1) {
	    ?>
		<style type="text/css" media="screen">
		<?php
		print($css_screen);
	    ?>
		</style>
		<style type="text/css" media="print">
		<?php
		print($css_print);
	    ?>
		</style>
		<script type="text/javascript">
		<?php
		print($js);
	    ?>
		</script>
		<?php
	}
	else {
	    ?>
		<style type="text/css" media="screen">
		@import url(/resume.php?output=css_screen);
	    </style>
		<link rel="stylesheet" type="text/css" media="print" href="/resume.php?output=css_print" />
		<script type="text/javascript" src="/resume.php?output=js"></script>
		<?php
	}
    ?>
	</head>

	<body>

	<h1><?php print(encode_output($bio['name'])); ?></h1>

	<p id="bio_left">
	<?php
	if ($prefs['show_phone'] == 1) {
	    print(encode_output($bio['phone']).'<br />');
	}
    if ($prefs['show_email'] == 1) {
	?>
	    <script type="text/javascript">email();</script>
	    <?php
    }
    ?>
	</p>
	<p id="bio_right">
	<?php
	if ($prefs['show_address'] == 1) {
	    print(encode_output($bio['address']).'<br />'
		    .encode_output($bio['city']).', '
		    .encode_output($bio['state']).' '
		    .encode_output($bio['zip'])
		 );
	}
    ?>
	</p>

	<h2>OBJECTIVE</h2>
	<p class="data"><?php print(encode_output($data['objective'])); ?></p>

	<h2>EXPERIENCE</h2>
	<?php
	foreach ($data['jobs'] as $job) { 
	    if ($job['show'] == 1) {
		if (!empty($job['employer_link'])) {
		    $link = array(
			    '<a href="'.$job['employer_link'].'">'
			    ,'</a>'
			    );
		}
		else {
		    $link = array('','');
		}
		?>
		    <div class="job">
		    <p class="date"><?php print(encode_output($job['period'])); ?></p>
		    <div class="job_data">
		    <h3><?php print($link[0].encode_output($job['employer']).$link[1]); ?></h3>
		    <?php
		    if (!empty($job['location'])) {
			?>
			    <p class="location"><?php print(encode_output($job['location'])); ?></p>
			    <?php
		    }
		    else {
			?>
			    <p class="location">&nbsp;</p>
			    <?php
		    }
		if (!empty($job['employer_info'])) {
		    ?>
			<p class="company"><?php print(encode_output($job['employer_info'])); ?></p>
			<?php
		}
		foreach ($job['positions'] as $title => $position) {
		    ?>
			<p class="position"><?php print(encode_output($title)); ?></p>
			<ul>
			<?php
			foreach ($position as $responsibility) {
			    print("			<li>".encode_output($responsibility)."</li>\n");
			}
		    ?>
			</ul>
			&nbsp;
		    <?php
		}
		?>
		    </div>
		    </div>

		    <?php
	    }
	}
    ?>
	<h2>SKILLS, TECHNOLOGIES &amp; PROJECTS</h2>
	<ul>
	<?php
	foreach ($data['skills_technologies_projects'] as $temp) {
	    print("	<li>".encode_output($temp)."</li>\n");
	}
    ?>
	</ul>

	<h2>EDUCATION &amp; INTERESTS</h2>
	<ul>
	<?php
	foreach ($data['education_interests'] as $temp) {
	    print("	<li>".encode_output($temp)."</li>\n");
	}
    ?>
	</ul>

	<!--<p id="references">LinkedIn Profile: <a href="http://www.linkedin.com/pub/2/a/731">http://www.linkedin.com/pub/2/a/731</a> - Direct references available upon request.</p>-->

	<p id="meta">
	<?php
	if (empty($_GET['contained']) || $_GET['contained'] != 1) {
	    ?>
		<span id="versions"><a href="./resume.php">HTML version</a> | <a href="./resume.pdf">E-mailable PDF version</a> | <a href="./resume.php?output=text">Plain Text (.txt) version</a> | </span>
		<?php
	}
    ?>
	<span id="valid">Validate <a href="http://validator.w3.org/check/referer">XHTML 1.1</a> / <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS 2.0</a></span>
	</p>

	</body>
	</html>
	<?php
	break;
    case "text":
	header("Content-type: text/plain");
    print($bio['name']."\n\n");
    if ($prefs['show_phone'] == 1) {
	print(str_pad($bio['phone'], 50, ' '));
    }
    if ($prefs['show_address'] == 1) {
	print(str_pad($bio['address'], 50, ' ', STR_PAD_LEFT)."\n"); 
    }
    if ($prefs['show_email'] == 1) {
	print(str_pad($bio['email'], 50, ' '));
    }
    if ($prefs['show_address'] == 1) {
	print(str_pad($bio['city'].', '.$bio['state'].' '.$bio['zip'], 50, ' ', STR_PAD_LEFT)); 
    }
    print("\n\n");
    print('OBJECTIVE'."\n".str_pad('', 100, '-')."\n");
    print(pad_and_wrap($data['objective'])."\n\n\n");
    print('EXPERIENCE'."\n".str_pad('', 100, '-')."\n");
    foreach ($data['jobs'] as $job) { 
	if ($job['show'] == 1) {
	    print(str_pad($job['period'], 20, ' '));
	    print(str_pad($job['employer'], 55, ' '));
	    print(str_pad($job['location'], 25, ' ', STR_PAD_LEFT)."\n");
	    if (!empty($job['employer_info'])) {
		print(pad_and_wrap($job['employer_info'])."\n");
	    }
	    foreach ($job['positions'] as $title => $position) {
		print("\n".pad_and_wrap($title)."\n");
		foreach ($position as $responsibility) {
		    print(pad_and_wrap($responsibility, 20, ' ', 80, '- ')."\n");
		}
	    }
	}
	print("\n");
    }
    print("\n".'SKILLS, TECHNOLOGIES & PROJECTS'."\n".str_pad('', 100, '-')."\n");
    foreach ($data['skills_technologies_projects'] as $temp) {
	print(pad_and_wrap($temp, 20, ' ', 80, '- ')."\n");
    }
    print("\n\n".'EDUCATION & INTERESTS'."\n".str_pad('', 100, '-')."\n");
    foreach ($data['education_interests'] as $temp) {
	print(pad_and_wrap($temp, 20, ' ', 80, '- ')."\n");
    }
    print("\n\n".'LinkedIn Profile: http://www.linkedin.com/pub/2/a/731 - Direct references available upon request.'."\n\n");
    break;
}
?>
